package com.philocare.ycblesdk;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.philocare.ycblesdk.adapter.DeviceAdapter;
import com.philocare.ycblesdk.model.ConnectEvent;
import com.yucheng.ycbtsdk.AITools;
import com.yucheng.ycbtsdk.Constants;
import com.yucheng.ycbtsdk.YCBTClient;
import com.yucheng.ycbtsdk.bean.ScanDeviceBean;
import com.yucheng.ycbtsdk.response.BleConnectResponse;
import com.yucheng.ycbtsdk.response.BleDataResponse;
import com.yucheng.ycbtsdk.response.BleRealDataResponse;
import com.yucheng.ycbtsdk.response.BleScanResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        TextToSpeech.OnInitListener, ServiceConnection {


    public static final int TIMEOUT_SECONDS = 25;

    private ListView listView;


    private List<ScanDeviceBean> listModel = new ArrayList<>();
    private List<String> listVal = new ArrayList<>();
    DeviceAdapter deviceAdapter = new DeviceAdapter(MainActivity.this, listModel);

    private AITools aiTools;

    private TextView dfuUpdateView;

    private String macVal;

    private String realData = "";
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            if (msg.what == 0) {
                handler.sendEmptyMessageDelayed(0, 1000);
                YCBTClient.getAllRealDataFromDevice(new BleDataResponse() {
                    @Override
                    public void onDataResponse(int i, float v, HashMap hashMap) {
                        if (hashMap != null) {
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((TextView) findViewById(R.id.tv_get_real)).setText(hashMap.toString());
                                }
                            });
                        }
                    }
                });
            }
            return false;
        }
    });

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //EventBus.getDefault().register(this);

        // Inicializa o serviço de bluetooth
        //startService(new Intent(this, MyBleService.class));
//        startService(new Intent(this, BackService.class));

        // Atribui a lista de dispositivos
        listView = findViewById(R.id.device_list_view);
        listView.setAdapter(deviceAdapter);

        // Atribui os botões da interface para respectivas atribuições

        // ###########################################################################
        // 18/01/2022
        // Vamos começar os estudos com o escaneamento das pulseiras

        // Inicializa a busca
        findViewById(R.id.bt_start_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartScan();
            }
        });
        // Pára a busca
        //findViewById(R.id.bt_stop_scan).setOnClickListener(this);


//        findViewById(R.id.bt_connect_dev).setOnClickListener(this);
//        findViewById(R.id.bt_write_test).setOnClickListener(this);
//        findViewById(R.id.bt_write_stop).setOnClickListener(this);
//        findViewById(R.id.bt_write_real).setOnClickListener(this);
//        findViewById(R.id.bt_ui_getinfo).setOnClickListener(this);
//        findViewById(R.id.bt_get_history_data).setOnClickListener(this);
//        findViewById(R.id.bt_delete_history_data).setOnClickListener(this);
//        findViewById(R.id.open_real_temp).setOnClickListener(this);
//        findViewById(R.id.read_real_temp).setOnClickListener(this);
//        findViewById(R.id.close_real_temp).setOnClickListener(this);
//        findViewById(R.id.bt_disconnect_dev).setOnClickListener(this);
//        findViewById(R.id.send_message).setOnClickListener(this);
//        findViewById(R.id.send_model).setOnClickListener(this);
//        findViewById(R.id.send_reset_order).setOnClickListener(this);
//        findViewById(R.id.dfu_update_view).setOnClickListener(this);
//        findViewById(R.id.start_sport).setOnClickListener(this);
//        findViewById(R.id.end_sport).setOnClickListener(this);
//        findViewById(R.id.get_real).setOnClickListener(this);
//        findViewById(R.id.watchDialDownload).setOnClickListener(this);
//        findViewById(R.id.history_data_view).setOnClickListener(this);
//        findViewById(R.id.history_blood_data_view).setOnClickListener(this);
//        findViewById(R.id.delete_history_data_view).setOnClickListener(this);
//        findViewById(R.id.delete_history_blood_data_view).setOnClickListener(this);
//        findViewById(R.id.start_test_blood_view).setOnClickListener(this);


//        String defaultMac = (String) SPHelper.get(MainActivity.this, "key", "no");
//
//        if (!defaultMac.equals("no")) {
//
//            Log.e("device", "default=" + defaultMac);
//
//
//            YCBTClient.connectBle(defaultMac, new BleConnectResponse() {
//                @Override
//                public void onConnectResponse(final int i) {
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            Toast.makeText(MainActivity.this, "main=" + i, Toast.LENGTH_SHORT).show();
//                            baseOrderSet();
//                        }
//                    });
//                }
//            });
//        }

        // Aqui tem a lista de dispositivos encontrados
        // Atribui se o clicque para a lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Clicou - Pára de buscar a pulseira
                YCBTClient.stopScanBle();

                // Pega da lista que buscou
                ScanDeviceBean scanDeviceBean = (ScanDeviceBean) parent.getItemAtPosition(position);

                // Salva no helper
                SPHelper.setParam(MainActivity.this, "key", scanDeviceBean.getDeviceMac());

                // pega o mac para solicitar a conexão
                macVal = scanDeviceBean.getDeviceMac();

                // Solicita o driver para conectar na pulseira
                YCBTClient.connectBle(scanDeviceBean.getDeviceMac(), new BleConnectResponse() {
                    @Override
                    public void onConnectResponse(final int i) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                    baseOrderSet();
                                Toast.makeText(MainActivity.this, "i=" + i, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)

    //############################################################################
    // 19/01/2022
    // Funções de acionamento dos botões
    private void StartScan() {
        // #######################################################################
        // O objeto YCBT Client está na bilioteca fornecida pela Multilaser
        YCBTClient.startScanBle(new BleScanResponse() {
            @Override
            public void onScanResponse(int i, ScanDeviceBean scanDeviceBean) {
                // Procura os dispositivos próximos
                if (scanDeviceBean != null) {
                    // A cada dispositivo encontrado ele adiciona a lista se já não tiver
                    // adicionado
                    // O MAC da pulseira é adicionado
                    if (!listVal.contains(scanDeviceBean.getDeviceMac())) {
                        listVal.add(scanDeviceBean.getDeviceMac());
                        deviceAdapter.addModel(scanDeviceBean);
                    }
                    Log.e("device", "mac=" + scanDeviceBean.getDeviceMac() + ";name=" + scanDeviceBean.getDeviceName() + "rssi=" + scanDeviceBean.getDeviceRssi());
                }
            }
        }, TIMEOUT_SECONDS);
    }


//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.get_real:
//                realData = "";
//                handler.removeMessages(0);
//                handler.sendEmptyMessage(0);
//                break;
//            case R.id.dfu_update_view:
//                Intent dfuIntent = new Intent(MainActivity.this, DfuUpdateActivity.class);
//                startActivity(dfuIntent);
//                break;
//            case R.id.bt_disconnect_dev:
//                YCBTClient.disconnectBle();
//                break;
//            case R.id.bt_start_scan: {
//
//                // #######################################################################
//                // O objeto YCBT Client está na bilioteca fornecida pela Multilaser
//                YCBTClient.startScanBle(new BleScanResponse() {
//                    @Override
//                    public void onScanResponse(int i, ScanDeviceBean scanDeviceBean) {
//                        // Procura os dispositivos próximos
//                        if (scanDeviceBean != null) {
//                            // A cada dispositivo encontrado ele adiciona a lista se já não tiver
//                            // adicionado
//                            // O MAC da pulseira é adicionado
//                            if (!listVal.contains(scanDeviceBean.getDeviceMac())) {
//                                listVal.add(scanDeviceBean.getDeviceMac());
//                                deviceAdapter.addModel(scanDeviceBean);
//                            }
//                            Log.e("device", "mac=" + scanDeviceBean.getDeviceMac() + ";name=" + scanDeviceBean.getDeviceName() + "rssi=" + scanDeviceBean.getDeviceRssi());
//                        }
//                    }
//                }, TIMEOUT_SECONDS);
//
//                break;
//            }
//            case R.id.bt_stop_scan: {
//                YCBTClient.stopScanBle();
//                break;
//            }
//            case R.id.watchDialDownload:
//                try {
//                    InputStream is = getResources().openRawResource(R.raw.night_15689745621);
//                    byte[] buffer = new byte[1024];
//                    int len = 0;
//                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                    while ((len = is.read(buffer)) != -1) {
//                        bos.write(buffer, 0, len);
//                    }
//                    bos.flush();
//                   /* YCBTClient.watchDialDownload(bos.toByteArray(), new BleDataResponse() {
//                        @Override
//                        public void onDataResponse(int i, float v, HashMap hashMap) {
//                            System.out.println("chong---------i==" + i);
//                        }
//                    });*/
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
//            case R.id.bt_connect_dev: {
//                //p3plus D8:D3:F7:1F:22:69
//                //v5 F3:E8:04:E8:73:68
////                YCBTClient.connectBle("", new BleConnectResponse() {
////                    @Override
////                    public void onConnectResponse(int i) {
////
////                    }
////                });
//                break;
//            }
//            case R.id.bt_write_test: {
//                //0x0200080047436FEC
//                AITools.getInstance().init();
//                AITools.getInstance().setAIDiagnosisHRVNormResponse(new BleAIDiagnosisHRVNormResponse() {
//                    @Override
//                    public void onAIDiagnosisResponse(HRVNormBean hrvNormBean) {
//                        /* float heavy_load;//carregando índice （Maior é melhor Saída）
//                         float pressure;//índice de estresse （Maior é melhor Saída）
//                         float HRV_norm;//Índice de VFC （maior é melhor de saída）
//                         float body;//índice corporal （maior é melhor de saída）
//                         int flag = -1;//0 OK -1 ERRO*/
//                        //System.out.println("chong--------AIDiagnosis");
//                    }
//                });
//                YCBTClient.appEcgTestStart(new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//
//                    }
//                }, new BleRealDataResponse() {
//                    @Override
//                    public void onRealDataResponse(int i, HashMap hashMap) {
//                        if (hashMap != null) {
//                            int dataType = (int) hashMap.get("dataType");
//                            Log.e("qob", "onRealDataResponse " + i + " dataType " + dataType);
//                            if (i == Constants.DATATYPE.Real_UploadECG) {
//                                final List<Integer> tData = (List<Integer>) hashMap.get("data");
//                                //System.out.println("chong----------ecgData==" + tData.toString());
//                                //一定要在主线程分析
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        Log.e("qob", "AI " + tData.size());
//                                    }
//                                });
//                            } else if (i == Constants.DATATYPE.Real_UploadPPG) {
//                                byte[] param = (byte[]) hashMap.get("data");
//                                Log.e("qob", "ppg: " + Arrays.toString(param));
//                            } else if (i == Constants.DATATYPE.Real_UploadECGHrv) {
//                                float param = (float) hashMap.get("data");
//                                Log.e("qob", "HRV: " + param);
//                            } else if (i == Constants.DATATYPE.Real_UploadECGRR) {
//                                float param = (float) hashMap.get("data");
//                                Log.e("qob", "RR invo " + param);
//                            } else if (i == Constants.DATATYPE.Real_UploadBlood) {
//                                int heart = (int) hashMap.get("heartValue");//Frequência cardíaca
//                                int tDBP = (int) hashMap.get("bloodDBP");//alta pressão
//                                int tSBP = (int) hashMap.get("bloodSBP");//Pressão baixa
//                            }
//                        }
//                    }
//                });
//                break;
//            }
//            case R.id.bt_write_stop: {
//                YCBTClient.appEcgTestEnd(new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        System.out.println("Chong ------ teste de teste i ==" + i);
//                        if (i != 0) {
//                            //测试异常
//                            return;
//                        }
//                        HRVNormBean bean = AITools.getInstance().getHrvNorm();
//                        if (bean != null) {
//                            if (bean.flag == -1) {
//                                //错误
//                            } else {//正常
//                                float heavy_load = bean.heavy_load;//O índice de carga (o maior mais não é bom)
//                                float pressure = bean.pressure;//Índice de pressão (maior, mais ausência)
//                                float HRV_norm = bean.HRV_norm;//Índice de HRV (maior e melhor)
//                                float body = bean.body;//Índice de corpo (maior e melhor)
//                            }
//                        }
//
//                        AITools.getInstance().getAIDiagnosisResult(new BleAIDiagnosisResponse() {
//                            @Override
//                            public void onAIDiagnosisResponse(AIDataBean aiDataBean) {
//                                if (aiDataBean != null) {
//                                    short heart = aiDataBean.heart;//Frequência cardíaca
//                                    int qrstype = aiDataBean.qrstype;//Tipo 1 Heart Shoot Normal 5 quartos cedo tiro 9 quarto cedo tiro 14 ruído
//                                    boolean is_atrial_fibrillation = aiDataBean.is_atrial_fibrillation;//Se uma fibrilação atrial
//                                    System.out.println("chong------heart==" + heart + "--qrstype==" + qrstype + "--is_atrial_fibrillation==" + is_atrial_fibrillation);
//                                }
//                            }
//                        });
//                    }
//                });
//                break;
//            }
//            case R.id.bt_write_real: {
//                YCBTClient.deviceToApp(new BleDeviceToAppDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, HashMap hashMap) {
//                        if (hashMap != null) {
//                            if (i == 0) {//
//                                int dataType = (int) hashMap.get("dataType");
//                                int data = -1;
//                                if (hashMap.get("data") != null){
//                                    data = (int) hashMap.get("data");
//                                }
//                                switch (dataType) {
//                                    case Constants.DATATYPE.AppECGPPGStatus:
//                                        int EcgStatus = (int) hashMap.get("EcgStatus");
//                                        int PPGStatus = (int) hashMap.get("PPGStatus");
//                                        if (PPGStatus == 0) {//0 :wear  1: no wear 2: error
//                                        }
//                                        break;
//                                    case Constants.DATATYPE.DeviceFindMobile://Procurando telefones celulares
//                                        if (data == 0) {//0: Fim 1: Início
//                                        }
//                                        break;
//                                    case Constants.DATATYPE.DeviceLostReminder://Lembrete de perda
//                                        if (data == 0) {//0: Fim 1: Início
//                                        }
//                                        break;
//                                    case Constants.DATATYPE.DeviceAnswerAndClosePhone://Resposta / recuse-se a atender o telefone
//                                        if (data == 0) {//0: Resposta 1: Rejeitar
//                                        }
//                                        break;
//                                    case Constants.DATATYPE.DeviceTakePhoto://Controle da câmera da câmera
//                                        if (data == 0) {//0x00: Sair do modo de câmera 0x01: Digite o modo Foto 0x02: Foto
//                                        }
//                                        break;
//                                    case Constants.DATATYPE.DeviceStartMusic://Controle de música
//                                        if (data == 0) {//0: Music Stop 1: Play 2: Pausa 3: Anterior 4: Next Song
//                                        }
//                                        break;
//                                    case Constants.DATATYPE.DeviceSos://Abra um comando Key Call Rescue
//                                        break;
//                                    case Constants.DATATYPE.DeviceDrinkingPatterns://Abra um comando de controle de modo de beber
//                                        break;
//                                    case Constants.DATATYPE.DeviceMeasurementResult://Pulseira Return App Iniciar Único Resultados de Medição
//                                        if(hashMap.get("datas") != null){
//                                            byte[] datas = (byte[]) hashMap.get("datas");
//                                            if((datas[0] & 0xff) == 0){//0x00: Frequência cardíaca 0x01: Pressão arterial 0x02: Oxigênio do sangue 0x03: Taxa de respiração 0x04: temperatura corporal
//                                                if((datas[1] & 0xff) == 0){//0x00: Medida de saída do usuário 0x01: Sucesso de medição 0x02: falha de medição
//                                                    //...
//                                                }
//                                            }
//                                        }
//                                        break;
//                                }
//                            }
//                        }
//                    }
//                });
//
//                break;
//            }
//            case R.id.bt_ui_getinfo: {
//                YCBTClient.getDeviceInfo(new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//
//                    }
//                });/*if(hashMap != null){
//                            HashMap map = (HashMap) hashMap.get("data");
//                            String deviceId = (String) map.get("deviceId");
//                            String deviceVersion = (String) map.get("deviceVersion");
//                            String deviceBatteryState = (String) map.get("deviceBatteryState");
//                            String deviceBatteryValue = (String) map.get("deviceBatteryValue");
//                        }*/
//                break;
//            }
//            case R.id.bt_get_history_data:
//                closeRegisterRealStepData();
//                YCBTClient.healthHistoryData(0x0509, new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        if (hashMap != null) {
//                            System.out.println("chong---------hashmap==" + hashMap.toString());
//                        }
//                    }
//                });
//                openRegisterRealStepData();
//                break;
//            case R.id.bt_delete_history_data:
//                YCBTClient.deleteHealthHistoryData(0x0544, new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        if (i == 0) {//delete success
//
//                        }
//                    }
//                });
//                break;
//            case R.id.read_real_temp://read real temp
//                readRealTemp();
//                break;
//            case R.id.open_real_temp://open real temp
//                openRealTemp();
//                break;
//            case R.id.close_real_temp://close real temp
//                closeRealTemp();
//                break;
//            case R.id.send_message:
//                YCBTClient.appSengMessageToDevice(0x03, "Tome remédio antes das refeições", "Exemplo: 1 penillin, 10 paracetamol", new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        if (i == 0) {
//                            //success sendMessage
//                        }
//                    }
//                });
//                break;
//            case R.id.send_model:
//                YCBTClient.appMobileModel("OPPO A77", new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        if (i == 0) {
//                            //success
//                            int sleepNum = (int) hashMap.get("SleepNum");
//                            int sleepTotalTime = (int) hashMap.get("SleepTotalTime");
//                            int heartNum = (int) hashMap.get("HeartNum");
//                            int sportNum = (int) hashMap.get("SportNum");
//                            int bloodNum = (int) hashMap.get("BloodNum");
//                            int bloodOxygenNum = (int) hashMap.get("BloodOxygenNum");
//                            int tempHumidNum = (int) hashMap.get("TempHumidNum");
//                            int tempNum = (int) hashMap.get("TempNum");
//                            int ambientLightNum = (int) hashMap.get("AmbientLightNum");
//                        }
//                    }
//                });
//                break;
//            case R.id.send_reset_order:
////                YCBTClient.settingRestoreFactory(new BleDataResponse() {
////                    @Override
////                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
////                        Log.e("cellhomepartssss",".......reset...order...");
////                    }
////                });
//
//
//                Intent otherIntent = new Intent(MainActivity.this, OtherActivity.class);
//                startActivity(otherIntent);
//                break;
//            case R.id.start_sport:
//                YCBTClient.appRunMode(0x01, 0x16, new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        System.out.println("chong---------run mode start==" + i);
//                    }
//                });
//                break;
//            case R.id.end_sport:
//                YCBTClient.appRunMode(0x00, 0x16, new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int i, float v, HashMap hashMap) {
//                        System.out.println("chong---------run mode end==" + i);
//                    }
//                });
//                break;
//            case R.id.history_data_view:
//                syncHistoryThreeTime();
//                break;
//            case R.id.history_blood_data_view:
//                syncHistoryBloodTime();
//                break;
//            case R.id.delete_history_data_view:
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        deleteThreeHistory(0xffffffff);
//                    }
//                }).start();
//                break;
//            case R.id.delete_history_blood_data_view:
//                deleteBloodHistory(0xffffffff);
//                break;
//            case R.id.start_test_blood_view:
//                if (YCBTClient.connectState() == ReadWriteOK) {
//                    startActivity(new Intent(MainActivity.this, TestBoolActivity.class));
//                }
//                break;
//        }
//    }

    //Obter Timestamp ECG.
    private void syncHistoryThreeTime() {
        Log.e("yc-ble", "Dados síncronos do sensor de aceleração de três eixos....");
        //Obter Timestamp PPG.
        YCBTClient.collectHistoryListData(0x02, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (hashMap != null) {
                    Log.e("historydata", "Dados históricos do acelerador de três eixos hashMap=" + hashMap.toString());
                    List<HashMap> data = (List<HashMap>) hashMap.get("data");
                    for (HashMap map : data) {
                        threeTimes.add((Long) map.get("collectSendTime"));
                    }
                    getThreeHistory();
                } else {
                    Log.e("historydata", "Não há dados históricos do sensor de aceleração de três eixos");
                }
            }
        });
    }

    private List<Long> threeTimes = new ArrayList<>();
    private List<Long> bloodTimes = new ArrayList<>();

    //Obter Timestamp PPG.
    private void syncHistoryBloodTime() {
        Log.e("yc-ble", "Dados insufláveis ​​síncronos da pressão arterial....");
        YCBTClient.collectHistoryListData(0x06, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (hashMap != null) {
                    Log.e("historydata", "Dados infláveis ​​históricos da pressão arterial hashMap=" + hashMap);
                    List<HashMap> data = (List<HashMap>) hashMap.get("data");
                    for (HashMap map : data) {
                        bloodTimes.add((Long) map.get("collectSendTime"));
                    }
                    getBloodHistory();
                } else {
                    Log.e("historydata", "Não há dados históricos de pressão arterial inflável");
                }
            }
        });
    }

    private void getThreeHistory() {
        if (threeTimes.size() > 0) {
            final long time = threeTimes.get(0);
            threeTimes.remove(0);
            YCBTClient.collectHistoryDataWithTimestamp(0x02, time, new BleDataResponse() {
                @Override
                public void onDataResponse(int code, float ratio, HashMap resultMap) {
                    Log.e("historydata", "Complete dados históricos aceleradores de três eixos hashMap=" + resultMap);
                    //YCBTLog.savaFile(String.format("%08X", time), ByteUtil.byteToStringSpace((byte[]) resultMap.get("data")));
                    getThreeHistory();
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Complete dados históricos aceleradores de três eixos", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void getBloodHistory() {
        if (bloodTimes.size() > 0) {
            final long time = bloodTimes.get(0);
            bloodTimes.remove(0);
            YCBTClient.collectHistoryDataWithTimestamp(0x06, time, new BleDataResponse() {
                @Override
                public void onDataResponse(int code, float ratio, HashMap resultMap) {
                    Log.e("historydata", "Dados de pressão arterial inflável histórica completa hashMap=" + resultMap);
                    byte[] data = (byte[]) resultMap.get("data");
                    long startTime = (time + YCBTClient.SecFrom30Year) * 1000 - TimeZone.getDefault().getOffset(System.currentTimeMillis());
                    String info = "";
                    if (data != null && data.length > 9) {
                        info = "pressão arterial" + "-" + (data[2] & 0xff) + "-" + (data[3] & 0xff) + "mmhg-"//(char) (data[1] & 0xff) + (char) (data[0] & 0xff) +
                                + (data[4] & 0xff) + "bpm-" + (data[5] & 0xff) + "cm-" + (data[6] & 0xff) + "kg-" + (data[7] & 0xff) + "era-" + (data[8] == 0 ? "macho" : "Fêmea");
                    }
                    String fileName = getDateToString(startTime) + " " + info + ".txt";
                    //YCBTLog.savaFile(fileName, ByteUtil.boolByteToString(data));
                    getBloodHistory();
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Dados de pressão arterial inflável histórica completa", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void deleteThreeHistory(long tim1e) {
        try {
            List<String> names = getFileName(isExistDir("health/dial"));
            for (String name : names) {
                FileInputStream inputStream = new FileInputStream(new File(isExistDir("health/dial") + "/" + name));
                String[] content = getString(inputStream).split(",");
                inputStream.close();
                int[] data = Arrays.asList(content).stream().mapToInt(Integer::parseInt).toArray();
                System.out.println("chong--------filename == " + name.substring(0, name.indexOf(".")));
                long startTime = (Long.parseLong(name.substring(0, name.indexOf(".")), 16) + YCBTClient.SecFrom30Year) * 1000L - TimeZone.getDefault().getOffset(System.currentTimeMillis());
                String info = "";
                if (data != null && data.length > 9) {
                    info = "pressão arterial" + "-" + (data[2] & 0xff) + "-" + (data[3] & 0xff) + "mmhg-"//(char) (data[1] & 0xff) + (char) (data[0] & 0xff) +
                            + (data[4] & 0xff) + "bpm-" + (data[5] & 0xff) + "cm-" + (data[6] & 0xff) + "kg-" + (data[7] & 0xff) + "岁-" + (data[8] == 0 ? "macho" : "Fêmea");
                }
                String fileName = getDateToString(startTime) + " " + info + ".txt";
                //YCBTLog.savaFile(fileName, ByteUtil.boolIntToString(data));
                System.out.println("Chong ------ Salvar arquivo -" + fileName + "==" + startTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Chong ----- Inicie o mostrador de instalação");
        }
        /*YCBTClient.deleteHistoryListData(0x02, time, new BleDataResponse() {
            @Override
            public void onDataResponse(int code, float ratio, HashMap resultMap) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "deletado com sucesso", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });*/
    }

    private void deleteBloodHistory(long time) {
        YCBTClient.deleteHistoryListData(0x06, time, new BleDataResponse() {
            @Override
            public void onDataResponse(int code, float ratio, HashMap resultMap) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "deletado com sucesso", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    public static String getDateToString(long milSecond) {
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public static String getString(InputStream inputStream) {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static List<String> getFileName(String fileAbsolutePath) {
        List<String> lists = new ArrayList<>();
        try {
            File[] subFile = new File(fileAbsolutePath).listFiles();
            if (subFile != null && subFile.length > 0) {
                for (File file : subFile) {
                    if (!file.isDirectory() && file.getName().endsWith(".txt")) {
                        lists.add(file.getName());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lists;
    }

    /**
     * After opening, the bracelet will be black. This is normal.
     */
    private void openRealTemp() {
        YCBTClient.appTemperatureMeasure(0x01, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (i == 0) {
                    //success
                }
            }
        });
    }

    /**
     * if your need more, you can loop through this method.
     * but it must be after start_real_temp method.
     */
    private void readRealTemp() {
        YCBTClient.getRealTemp(new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (i == 0) {
                    String temp = (String) hashMap.get("tempValue");
                }
            }
        });
    }

    /**
     * If you call the startRealTemp() method, you must call this method.
     * Otherwise, the bracelet will always be black and the temperature will be monitored in the background.
     */
    private void closeRealTemp() {
        YCBTClient.appTemperatureMeasure(0x00, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (i == 0) {
                    //success
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void connectEvent(ConnectEvent connectEvent) {
        Log.e("mainorder", ".....connectent...........");
        baseOrderSet();
        Intent timeIntent = new Intent(MainActivity.this, ChoseActivity.class);
        timeIntent.putExtra("mac", macVal);
        //(timeIntent);
    }


    /***
     * configurações de linguagem
     * 0x00: Inglês 0x01: Chinês 0x02: Russo 0x03: Alemão 0x04: Francês
     * 0x05: Japonês 0x06: Espanhol 0x07: Italiano 0x08: Português
     * 0x09: coreano 0x0a: polonês 0x0b: malaio 0x0c: chinês tradicional 0xff: outros
     * @param
     */
    //Configurações básicas da diretiva.
    private void baseOrderSet() {


        /***
         * configurações de linguagem
         * @Param langtype 0x00: Inglês 0x01: Chinês 0x02: Russo 0x03: Alemão 0x04: Francês
         * 0x05: Japonês 0x06: Espanhol 0x07: Italiano 0x08: Português
         * 0x09: coreano 0x0a: polonês 0x0b: malaio 0x0c: chinês tradicional 0xff: outros
         * @param dataResponse
         */
        YCBTClient.settingLanguage(0x01, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "同步语言结束");
            }
        });


//        setPhoneTime();


        //Coleção de freqüência cardíaca
        YCBTClient.settingHeartMonitor(0x01, 10, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "设置10分钟间隔采集心率");
            }
        });


       /* //Nenhuma detecção de sentido
        YCBTClient.settingPpgCollect(0x01, 60, 60, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "Definir nenhuma aquisição de dados");
            }
        });


        //Frequência cardíaca síncrona
        syncHisHr();
        //Sono síncrono
        syncHisSleep();

        syncHisStep();*/


    }


    private void setPhoneTime() {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        int week2 = calendar.get(Calendar.DAY_OF_WEEK);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        int sec = calendar.get(Calendar.SECOND);

        if (week == 1) {
            week += 5;
        } else {
            week -= 2;
        }


        // semana 0-6 (segunda-feira ~ domingo)
        // 0 Segunda-feira, 1 terça-feira, 2 quarta-feira, 3 quinta-feira, 4ª sexta-feira, 5 sábado, 6 domingo
        Log.e("device", "day of week jian=" + week);
        Log.e("device", "day of week week2=" + week2);

//        YCBTClient.getDeviceInfo(new BleDataResponse() {
//            @Override
//            public void onDataResponse(int i, float v, HashMap hashMap) {
//
//            }
//        });

    }


    //Taxa cardíaca histórica síncrona
    private void syncHisHr() {
        YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistoryHeart, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {

                if (hashMap != null) {

                    Log.e("history", "hashMap=" + hashMap.toString());

                    Log.e("history", "hr time=" + hashMap.get("heartStartTime"));

                    Log.e("history", "hr val=" + hashMap.get("heartValue"));
                } else {
                    Log.e("history", "no ..hr..data....");
                }

//                syncHisStep();
            }
        });
    }


    //Histórico síncrono sono
    private void syncHisSleep() {
        YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistorySleep, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {

                if (hashMap != null) {
                    Log.e("history", "hashMap=" + hashMap.toString());

                } else {
                    Log.e("history", "no ..sleep..data....");
                }

                //Passo Síncrono
//                syncHisStep();
//                hashMap.get();
            }
        });
    }

    //Etapas históricas síncronas
    private void syncHisStep() {
        YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistorySport, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (hashMap != null) {

                    Log.e("history", "hashMap=" + hashMap.toString());

                    Log.e("history", "step start time=" + hashMap.get("sportStartTime"));
                    Log.e("history", "step end time=" + hashMap.get("sportEndTime"));
                    Log.e("history", "step num=" + hashMap.get("sportStep"));
                } else {
                    Log.e("history", "no...step ..data....");
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void closeRegisterRealStepData() {
        YCBTClient.appRealSportFromDevice(0x00, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                //Log.e(TAG,"....");
                //healthHistoryData();
            }
        });
    }

    private void openRegisterRealStepData() {
        YCBTClient.appRealSportFromDevice(0x01, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                //Log.e(TAG,"....");
            }
        });
        YCBTClient.appRegisterRealDataCallBack(new BleRealDataResponse() {
            @Override
            public void onRealDataResponse(int i, HashMap hashMap) {
                if (i == Constants.DATATYPE.Real_UploadSport) {
                    if (hashMap != null && hashMap.size() > 0) {
                        int sportStep = (int) hashMap.get("sportStep");
                        int sportDistance = (int) hashMap.get("sportDistance");
                        int sportCalorie = (int) hashMap.get("sportCalorie");
                        //Log.e(TAG,"Passos de escuta em tempo real sportStep = "+sportStep+" ,sportDistance = " + sportDistance+" ,sportCalorie = "+sportCalorie);
                    }
                }//else if(i == Constants.DATATYPE.Real_UploadOGA){

                //}

            }
        });
    }

    private String isExistDir(String saveDir) throws IOException {
        // Download Localização
        File downloadFile = new File(Environment.getExternalStorageDirectory(), saveDir);
        if (!downloadFile.mkdirs()) {
            downloadFile.createNewFile();
        }
        return downloadFile.getAbsolutePath();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public void onInit(int status) {

    }

    @Override
    public void onRefresh() {

    }
}
