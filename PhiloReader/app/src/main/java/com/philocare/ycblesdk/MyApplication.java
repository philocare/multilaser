package com.philocare.ycblesdk;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.realsil.sdk.core.RtkConfigure;
import com.realsil.sdk.core.RtkCore;
import com.realsil.sdk.dfu.RtkDfu;
import com.yucheng.ycbtsdk.YCBTClient;
import com.yucheng.ycbtsdk.response.BleConnectResponse;
import com.yucheng.ycbtsdk.response.BleDeviceToAppDataResponse;

import java.util.HashMap;

public class MyApplication extends Application {

    private static MyApplication instance = null;




    public static MyApplication getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        String currentProcessName = getCurProcessName(this);

        if ("com.philocare.ycblesdk".equals(currentProcessName)) {

            Log.e("device","...onCreate.....");

            instance = this;
            YCBTClient.initClient(this,true);
            YCBTClient.registerBleStateChange(bleConnectResponse);
            YCBTClient.deviceToApp(toAppDataResponse);


            //Inicialização de atualização de firmware
            RtkConfigure configure = new RtkConfigure.Builder()
                    .debugEnabled(true)
                    .printLog(true)
                    .logTag("OTA")
                    .build();
            RtkCore.initialize(this, configure);
            RtkDfu.initialize(this, true);



        }
    }







//    BleConnectResponse bleConnectResponse = new BleConnectResponse() {
//        @Override
//        public void onConnectResponse(int var1) {
//
//            //Toast.makeText(MyApplication.this, "i222=" + var1, Toast.LENGTH_SHORT).show();
//
//            Log.e("device","...connect..state....." + var1);
//
//            if(var1 == 0){
//                EventBus.getDefault().post(new ConnectEvent());
//            }
//        }
//    };



    BleDeviceToAppDataResponse toAppDataResponse = new BleDeviceToAppDataResponse(){

        @Override
        public void onDataResponse(int dataType, HashMap dataMap) {

            Log.e("TimeSetActivity","Dados de retorno passivo。。。");
            Log.e("TimeSetActivity",dataMap.toString());

        }
    };


    boolean isActiveDisconnect = false;
    BleConnectResponse bleConnectResponse = new BleConnectResponse() {
        @Override
        public void onConnectResponse(int code) {
//            Toast.makeText(MyApplication.this, "i222=" + var1, Toast.LENGTH_SHORT).show();

            Log.e("deviceconnect", "Retorno global do monitor=" + code);

            if (code == com.yucheng.ycbtsdk.Constants.BLEState.Disconnect) {
//                thirdConnect = false;
//                BangleUtil.getInstance().SDK_VERSIONS = -1;
//                EventBus.getDefault().post(new BlueConnectFailEvent());
                /*if(SPUtil.getBindedDeviceMac() != null && !"".equals(SPUtil.getBindedDeviceMac())){
                    YCBTClient.connectBle(SPUtil.getBindedDeviceMac(), new BleConnectResponse() {
                        @Override
                        public void onConnectResponse(int code) {

                        }
                    });
                }*/
            } else if (code == com.yucheng.ycbtsdk.Constants.BLEState.Connected) {

            } else if (code == com.yucheng.ycbtsdk.Constants.BLEState.ReadWriteOK) {
//                thirdConnect = true;
//                BangleUtil.getInstance().SDK_VERSIONS = 3;
//                Log.e("deviceconnect", "A conexão Bluetooth foi bem-sucedida，monitoramento global");
//                setBaseOrder();
                //EventBus.getDefault().post(new ConnectEvent());
            } else {
                //code == Constants.BLEState.Disconnect
//                thirdConnect = false;
//                BangleUtil.getInstance().SDK_VERSIONS = -1;
//                EventBus.getDefault().post(new ConnectEvent());
            }
        }
    };


    /**
     * Obter o nome do processo atual
     *
     * @param context
     * @return
     */
    private String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }






}
