package com.philocare.ycblesdk;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.philocare.ycblesdk.model.BandBaseInfo;
import com.philocare.ycblesdk.model.HistEcgResponse;
import com.google.gson.Gson;
import com.yucheng.ycbtsdk.Constants;
import com.yucheng.ycbtsdk.response.BleDataResponse;
import com.yucheng.ycbtsdk.YCBTClient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class OldActivity extends Activity {

    public static String Tag = OldActivity.class.getName();

    private TextView timeSetView;
    private TextView batteryView;
    private TextView tiwenView;

    private TextView historyHrView;
    private TextView historySleepView;
    private TextView historySportView;

    private TextView hisEcgPpgView;

    private TextView xinLvNoticeView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old);
        initView();
    }

    private void initView(){
        timeSetView = (TextView) this.findViewById(R.id.time_set_view);
        batteryView = (TextView) this.findViewById(R.id.time_battery_view);
        tiwenView = (TextView) this.findViewById(R.id.time_tmpturedata_view);

        historyHrView = (TextView) this.findViewById(R.id.get_history_hr_view);
        historySleepView = (TextView) this.findViewById(R.id.get_history_sleep_view);
        historySportView = (TextView) this.findViewById(R.id.get_history_sport_view);

        hisEcgPpgView = (TextView)this.findViewById(R.id.get_history_ecgppg_view);
        xinLvNoticeView = (TextView)this.findViewById(R.id.xinlv_notice_view);

        //Definir modo de tempo 0x01 12 horas, 0x00 24 horas
        timeSetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingUnit(0, 0x00, 0x00, 0x01, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        if(code == 0){
                            Log.e("TimeSetActivity", "Modo de tempo definindo sucesso=");
                        }else {
                            Log.e("TimeSetActivity", "A configuração do modo de tempo falhou=");
                        }
                    }
                });
            }
        });


        //Obter as informações básicas do equipamento de acesso da bateria; ID do dispositivo, número de versão do firmware, status da bateria, energia da bateria, estado de ligação
        batteryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.getDeviceInfo(new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        if(code == 0){
                            if (resultMap != null) {

                                String backVal = resultMap.toString();
                                Gson gson = new Gson();
                                BandBaseInfo bandBaseInfo = gson.fromJson(backVal, BandBaseInfo.class);
                                Log.e(Tag, "resultMap=" + resultMap.toString());
                                Toast.makeText(OldActivity.this,"电量获取="+ bandBaseInfo.getData().getDeviceBatteryValue() + ";deviceVersion=" + bandBaseInfo.getData().getDeviceVersion(),Toast.LENGTH_SHORT).show();
//                                Log.e(Tag, "...Aquisição de eletricidade" + ";deviceBatteryValue=" + bandBaseInfo.getData().getDeviceBatteryValue() + ";deviceVersion=" + bandBaseInfo.getData().getDeviceVersion());
                            }
                        }else {
                            Toast.makeText(OldActivity.this,"获取电量失败"+code,Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });


        //Aquisição de dados de temperatura corporal
        tiwenView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(0x051E, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        if (resultMap != null) {
                            Log.e(Tag, "Obter temperatura corporal ..." + resultMap.toString());
//                            try {
//                                ArrayList<HashMap> lists = (ArrayList<HashMap>) resultMap.get("data");
//                                if (lists != null) {
//                                    String temp = "";
//                                    List<TmpBackBean> listTmpBeans = new ArrayList<>();
//                                    for (HashMap map : lists) {
//                                        if(map != null){
//                                            int tempIntValue = (int) map.get("tempIntValue");
//                                            int tempFloatValue = (int) map.get("tempFloatValue");
//                                            long startTime = (long) map.get("startTime");
//
//                                            if (tempFloatValue != 15) {
//                                                temp = tempIntValue + "." + tempFloatValue;
//
//                                                TmpBackBean tmpBackModel = new TmpBackBean();
//                                                tmpBackModel.setTimeVa(startTime);
//                                                tmpBackModel.setTmpVal(temp);
//                                                listTmpBeans.add(tmpBackModel);
//                                            }
//                                            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(startTime));
//                                            Log.e(Tag, "temp=" + temp + ";time=" + time);
//                                        }
//                                    }
//                                }
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
                        } else {
                            Log.e(Tag, "Não recebeu dados de temperatura corporal ...");
                        }
                    }
                });
            }
        });


        historyHrView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistoryHeart, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int i, float v, HashMap hashMap) {

                        Log.e(Tag, "Freqüência cardíaca síncrona...." + hashMap.toString());
                    }
                });

            }
        });

        historySleepView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(0x0504, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int i, float v, HashMap hashMap) {

                        Log.e(Tag, "sleep hashMap=" + hashMap.toString());

                    }
                });
            }
        });

        historySportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistorySport, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int i, float v, HashMap hashMap) {
                        Log.e(Tag, "Passo síncrono...." + hashMap.toString());
                    }
                });
            }
        });


        hisEcgPpgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncHistoryEcgTime();
            }
        });


        xinLvNoticeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingHeartAlarm(1, 10, 50, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Alarme de frequência cardíaca....code=" + code);
                    }
                });
            }
        });

    }




    private List<Long> ecgUseTimeVals = new ArrayList<>();
    private List<Long> ppgUseTimeVals = new ArrayList<>();

    private List<Long> ecgTimeVals = new ArrayList<>();
    private List<Long> ppgTimeVals = new ArrayList<>();

    int doubleType = 0;

    //Obter Timestamp ECG.
    private void syncHistoryEcgTime() {

        Log.e("yc-ble", "História Síncrona ECG....");

        //Obter Timestamp ECG.
        YCBTClient.collectEcgList(new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (hashMap != null) {
                    ecgTimeVals.clear();
                    Log.e("historydata", "his ecg hashMap=" + hashMap.toString());
                    String backVal = hashMap.toString();
                    Gson gson = new Gson();
                    HistEcgResponse response = gson.fromJson(backVal, HistEcgResponse.class);
                    for (int k = 0; k < response.getData().size(); k++) {
                        List<HistEcgResponse.HisEcgModel> dataVal = response.getData();
                        ecgTimeVals.add(dataVal.get(k).getCollectSendTime());
                    }
                    syncHistoryPpgTime();
                } else {
                    Log.e("historydata", "História Síncrona ECG Não Seu ECG");
                    syncHistoryPpgTime();
                }
            }
        });
    }


    //Obter Timestamp PPG.
    private void syncHistoryPpgTime() {

        Log.e("yc-ble", "PPG histórico síncrono....");

        YCBTClient.collectPpgList(new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (hashMap != null) {
                    ppgTimeVals.clear();
                    Log.e("historydata", "his ecg hashMap=" + hashMap.toString());
                    String backVal = hashMap.toString();

                    Gson gson = new Gson();
                    HistEcgResponse response = gson.fromJson(backVal, HistEcgResponse.class);

                    for (int k = 0; k < response.getData().size(); k++) {
                        List<HistEcgResponse.HisEcgModel> dataVal = response.getData();
                        ppgTimeVals.add(dataVal.get(k).getCollectSendTime());
                    }
                    timeHandle();
                } else {

                    Log.e("historydata", "História Síncrona PPG Não Seu ECG, Stop History ECG, PPG Sincronização");

//                    timeHandle();
                }
            }
        });
    }


    //Tempo de processamento
    private void timeHandle() {

        Log.e("historydata", "....timeHandle.....");

        if (ppgTimeVals.size() == 0 && ecgTimeVals.size() == 0) {
            //Nenhum timestamp, sair do seguinte julgamento lógica
            uploadDoubleFile();
//            hideHeaderData();
            return;
        }

        if (ecgTimeVals.size() > 0 && ppgTimeVals.size() == 0) {
            //Apenas ECG, não há PPG correspondente, ligue para excluir ECG inútil
            deleteNoUseEcg(ecgTimeVals);
//            hideHeaderData();
            return;
        }

        for (int i = 0; i < ppgTimeVals.size(); i++) {
            Long ecgTimecurrent = ppgTimeVals.get(i);
            if (ecgTimeVals.contains(ecgTimecurrent)) {
                ecgUseTimeVals.add(ecgTimecurrent);
            } else {
                ppgUseTimeVals.add(ecgTimecurrent);
            }
        }

        Log.e("dataRes2", "ecgusetime=" + ecgUseTimeVals.toString());
        Log.e("dataRes2", "ppgusetime=" + ppgUseTimeVals.toString());

//        uploadTotalNum = ecgUseTimeVals.size() + ppgUseTimeVals.size();
//        handleUploadData(ecgUseTimeVals.size() + ppgUseTimeVals.size());

        //Inicie o upload de dados com base no ponto de tempo
        handleDataBytime();
    }


    //Iniciar o upload de dados upload
    private void handleDataBytime() {

        if (ecgUseTimeVals.size() == 0) {
//            Log.e("dataRes2", "O par de sincronização de dados termina");
            //Inicie um único teste
            handleSinglePPgBytime();
            return;
        }

        //Indica que agora inicia a sincronização
        for (int i = 0; i < ecgUseTimeVals.size(); i++) {
            //Iniciar dados de sincronização
            doubleType = 0;
            beginUploadEcgData(ecgUseTimeVals.get(i));
            break;
        }
    }


    //Inicie o arquivo PPG único de sincronização
    private void handleSinglePPgBytime() {

        if (ppgUseTimeVals.size() == 0) {
//            Log.e("dataRes2", "Todos os dados são sincronizados");

            ecgUseTimeVals.clear();
            ppgUseTimeVals.clear();

            //Inicie a operação de upload de arquivos locais
            uploadDoubleFile();
            return;
        }


        for (int i = 0; i < ppgUseTimeVals.size(); i++) {
//            Log.e("dataRes2", "Inicie uma única sincronização de PPG");
            beginUploadPpgData(ppgUseTimeVals.get(i));

            break;
        }
    }


    //Upload de tempo
    public void uploadDoubleFile() {
        List<String> doubleFileName = getDoubleFile();
        for (int i = 0; i < doubleFileName.size(); i++) {
//            Log.e("dataRes", " Iniciar envio ： " + doubleFileName.get(i));
            handleDoubleFileName(doubleFileName.get(i));
            try {

                Random rand = new Random();
                int timecount = rand.nextInt(200) + 300;
                Thread.sleep(timecount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        Log.e("dataRes", "Fim do upload");
        //Começar a fazer o upload
        uploadSingleFile();
    }

    //Excluir ECG inútil, aqui há bug
    private void deleteNoUseEcg(List<Long> ecgNoUseTimeVals) {

        for (int i = 0; i < ecgNoUseTimeVals.size(); i++) {
            long nousertime = ecgNoUseTimeVals.get(i);
            YCBTClient.collectDeleteEcgPpg((int) nousertime, new BleDataResponse() {
                @Override
                public void onDataResponse(int i, float v, HashMap hashMap) {
//                    handleUploadData(ecgUseTimeVals.size() + ppgUseTimeVals.size());
                    handleSinglePPgBytime();
                }
            });
        }
    }


    //Obtenha um único conteúdo de arquivo ECG
    private void beginUploadEcgData(final long ecgTimeVal) {

        Log.e("dataRes2", "Inicie a sincronização ecg time=" + ecgTimeVal);

        YCBTClient.collectEcgDataWithTimestamp(ecgTimeVal, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {

                if (hashMap != null) {
                    byte[] tEcgData = (byte[]) hashMap.get("data");
                    save2(String.valueOf(sendTime((int) ecgTimeVal)), tEcgData);

//                    Log.e("dataRes2", TimeUtil.timeConverTimes(sendTime((int) ecgTimeVal)));

                    beginUploadDoublePpgData(ecgTimeVal);
                } else {
                    Log.e("historyEcg", "no his ecg data");
                    //Nenhum ECG correspondente, obtenha o PPG correspondente
                    beginUploadDoublePpgData(ecgTimeVal);
                }
            }
        });
    }


    private void save2(final String fileNameVal,final byte[] historyEcgDatas) {

//        ecgTempDataContain.clear();
//        ecgTempDataContain.addAll(historyEcgDatas);

        new Thread(new Runnable() {
            @Override
            public void run() {
                File file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/double");
                if (!file.exists()) {
                    file.mkdirs();
                }
                String ecgName = fileNameVal + ".ecg200";
                File ecgFile = new File(file, ecgName);
                try {
                    ecgFile.createNewFile();
                    FileOutputStream ecgFos = new FileOutputStream(ecgFile);
                    ecgFos.write(historyEcgDatas);
                    ecgFos.flush();
                    ecgFos.close();

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
//                    uploadDoubleFile();
                }
            }
        }).start();
    }



    private void beginUploadPpgData(final long ppgTimeval) {
//        myBleService.write(ProtocolWriter.getHistoryDataWithTime(1, currentSingleFileName));
//        Log.e("dataRes", "单个ppg time=" + ppgTimeval);

        try {
            YCBTClient.collectPpgDataWithTimestamp( ppgTimeval, new BleDataResponse() {
                @Override
                public void onDataResponse(int i, float v, HashMap hashMap) {

                    if (hashMap != null) {
                        byte[] tEcgData = (byte[]) hashMap.get("data");
                        ppgUseTimeVals.remove(ppgTimeval);


                        save(String.valueOf(sendTime((int) ppgTimeval)), 1, tEcgData);

                        //Espere 1 segundo, comece a excluir
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                deletePPgDataByTime(ppgTimeval);
//                            }
//                        }, 500);

                        deletePPgDataByTime(ppgTimeval);

                        Log.e("historyEcg", "ppgg data" + Arrays.toString(tEcgData));
                    } else {
                        Log.e("historyEcg", "no his ppg data");
                    }
                }
            });

        } catch (Exception e) {
            Log.e("dataRes", "..... Erro de dados único síncrono ppg ....." + e.toString());
            ppgUseTimeVals.remove(ppgTimeval);
            //Espere 1 segundo, comece a excluir
            deletePPgDataByTime(ppgTimeval);
        }
    }


    private void deletePPgDataByTime(long deleteTime) {
        Log.e("dataRes", "Único ppg. deleteTime=" + deleteTime);
        try {

            YCBTClient.collectDeleteEcgPpg((int) deleteTime, new BleDataResponse() {
                @Override
                public void onDataResponse(int i, float v, HashMap hashMap) {
//                    handleUploadData(ecgUseTimeVals.size() + ppgUseTimeVals.size());
                    handleSinglePPgBytime();
                }
            });
        } catch (Exception e) {
//            Log.e("dataRes", "..... delete um único ppg ....." + e.toString());
//            handleUploadData(ecgUseTimeVals.size() + ppgUseTimeVals.size());
            handleSinglePPgBytime();
        }
    }

    //开始成对中的Ppg上传
    private void beginUploadDoublePpgData(final long ppgTimeVal) {

        Log.e("dataRes2", "Inicie a sincronização PPG time=" + ppgTimeVal);
        //Sincronize os dados do ponto de tempo especificado com base no timestamp
        try {
            YCBTClient.collectPpgDataWithTimestamp( ppgTimeVal, new BleDataResponse() {
                @Override
                public void onDataResponse(int i, float v, HashMap hashMap) {

                    if (hashMap != null) {
                        byte[] tEcgData = (byte[]) hashMap.get("data");

                        ecgUseTimeVals.remove(ppgTimeVal);
//                                    Log.e("dataRes", ".Inicie a sincronização PPG...size=" + listECG.size());
                        save(String.valueOf(sendTime((int) ppgTimeVal)), 0, tEcgData);

//                        Log.e("dataRes2", TimeUtil.timeConverTimes(sendTime((int) ppgTimeVal)));


                        //Espere 1 segundo, comece a excluir
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                            }
//                        }, 1000);

                        deleteEcgDataByTime(ppgTimeVal);

                        Log.e("historyEcg", "ppgg data" + Arrays.toString(tEcgData));
                    } else {
                        Log.e("historyEcg", "no his ppg data");
                    }
                }
            });
        } catch (Exception e) {
//            e.printStackTrace();
            Log.e("dataRes", "..... Erro de dados de PPG síncrono ....." + e.toString());
//            deleteEcgDataByTime(ppgTimeVal);
            ecgUseTimeVals.remove(ppgTimeVal);
            //Espere 1 segundo, comece a excluir
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    deleteEcgDataByTime(ppgTimeVal);
//                }
//            }, 1000);

            deleteEcgDataByTime(ppgTimeVal);
        }
    }



    private long sendTime(long stime) {
//        long stime = 5591791;
//        int millisFromGMT = TimeZone.getDefault().getOffset(System.currentTimeMillis());
//        long tStartTime2 = (stime + YCBTClient.SecFrom30Year) * 1000;
        long tStartTime2 = (stime + YCBTClient.SecFrom30Year);
        return tStartTime2;
    }


    private void deleteEcgDataByTime(long deleteTime) {

        try {
            Log.e("dataRes", "Excluir ponto emparelhado no tempo=" + deleteTime);
            YCBTClient.collectDeleteEcgPpg((int) deleteTime, new BleDataResponse() {
                @Override
                public void onDataResponse(int i, float v, HashMap hashMap) {
//                    handleUploadData(ecgUseTimeVals.size() + ppgUseTimeVals.size());
                    handleDataBytime();
                }
            });
        } catch (Exception e) {
//            Log.e("dataRes", "..... Exclua Erro ECG ....." + e.toString());
//            handleUploadData(ecgUseTimeVals.size() + ppgUseTimeVals.size());
            handleDataBytime();
        }

    }


    private List<String> getDoubleFile() {
        List<String> fileLists = new ArrayList<>();
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/double");
            if (file.exists()) {
                File[] subFile = file.listFiles();

                for (int iFileLength = 0; iFileLength < subFile.length; iFileLength++) {
                    // Determine se é uma pasta
                    if (!subFile[iFileLength].isDirectory()) {
                        String filename = subFile[iFileLength].getName();
                        if (!TextUtils.isEmpty(filename)) {
                            String aftername = filename.substring(0, filename.lastIndexOf("."));
                            fileLists.add(aftername);
                        }
                    }
                }
                HashSet hashSet = new HashSet(fileLists);
                fileLists.clear();
                fileLists.addAll(hashSet);
//                Log.e("fileeee", " doulbe nome do arquivo ： " + fileLists.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileLists;
    }

    private void handleDoubleFileName(String fileName) {

        File file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/double");

//        Log.e("dataRes2", "fileName=" + TimeUtil.timeConverTimes(Long.parseLong(fileName)));

        String ecgFileVal = fileName + ".ecg200";
        String ppgFileVal = fileName + ".ppg200";

        File ecgFile = new File(file, ecgFileVal);
        File ppgFile = new File(file, ppgFileVal);

        if (ecgFile.exists() && ppgFile.exists()) {
//            uploadDoubleFIle(ecgFile, ppgFile);
        }
    }


    //Processando uma única vez
    private void handleSingleFileName(String fileName) {
        File file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/single");
        String ppgFileVal = fileName + ".ppg200";
        File ppgFile = new File(file, ppgFileVal);
        if (ppgFile.exists()) {
//            uploadSingleFIle(ppgFile);
        }
    }



    //Carregar um único PPG
//    private void uploadSingleFIle(File ppgNew) {
//
////        String userid = ACache.get(MyBleServiceFive.this).getAsString(MsgNum.AC_USER_ID);
////        String tokenVal = ACache.get(MyBleServiceFive.this).getAsString(MsgNum.AC_TOKEN_OLD);
//
//        int userid = (Integer) UserSPHelper.get(MyBleServiceFive.this, "userid", 0);
//        String tokenVal = (String) UserSPHelper.get(MyBleServiceFive.this, "token", "no");
//
//        HttpParams paramsPost = new HttpParams();
//        paramsPost.put("userId", userid);
//        paramsPost.put("filePpg200", ppgNew);
//        paramsPost.put("token", tokenVal);
//        paramsPost.put("version", "2");
//        //Upload de dados incomuns PPG
//        paramsPost.put("uploadType", "noninductive");
//        paramsPost.put("appVersion", "3.0");
//
//
//        OkGo.<String>post(Urls.BASE_URL_OLD + "/" + UrlPath.PATH_UPLOAD_PPG_FILE_URL).headers("Authorization", tokenVal)
//                .tag(MyBleServiceFive.this)
//                .params(paramsPost)
//                .isMultipart(true)
//                .execute(new StringCallback() {
//                    @Override
//                    public void onSuccess(Response<String> backresponse) {
//                        if (backresponse != null) {
//                            String backVal = backresponse.body();
//                            if (backVal != null) {
//                                Gson gson = new Gson();
//                                UploadResponse response = gson.fromJson(backVal, UploadResponse.class);
//                                if (response != null) {
//                                    if (response.getCode() == 200) {
////                                        Toast.makeText(MyBleService.this, "Carregar sucesso", Toast.LENGTH_SHORT).show();
//
//                                        deleteSingleFileByTime(ppgNew);
//
//                                        //No final, sob vibração
//                                    } else if (response.getStatus() == 1 && response.getMsg() != null && response.getMsg().equals("Erro de fósforos de token.")) {
//                                        Toast.makeText(MyBleServiceFive.this, response.getMsg(), Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        Toast.makeText(MyBleServiceFive.this, response.getMsg(), Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        super.onFinish();
//                    }
//                });
//    }




    //Upload de tempo único
    public void uploadSingleFile() {
        List<String> singleFile = getSingleFile();
        for (int i = 0; i < singleFile.size(); i++) {
//            Log.e("dataRes", " Inicie um único upload ： " + singleFile.get(i));
            handleSingleFileName(singleFile.get(i));
            try {
                Random rand = new Random();
                int timecount = rand.nextInt(200) + 300;
                Thread.sleep(timecount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        EventBus.getDefault().post(new HistoryUploadEvent());
//        Log.e("dataRes", "Entrada de upload único");
    }


    private List<String> getSingleFile() {
        List<String> fileLists = new ArrayList<>();
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/single");
            if (file.exists()) {
                File[] subFile = file.listFiles();

                for (int iFileLength = 0; iFileLength < subFile.length; iFileLength++) {
                    // Determine se é uma pasta
                    if (!subFile[iFileLength].isDirectory()) {
                        String filename = subFile[iFileLength].getName();
                        if (!TextUtils.isEmpty(filename)) {
                            String aftername = filename.substring(0, filename.lastIndexOf("."));
                            fileLists.add(aftername);
                        }
//                        Log.e("fileeee", "single nome do arquivo ： " + filename);
                    }
                }
                HashSet hashSet = new HashSet(fileLists);
                fileLists.clear();
                fileLists.addAll(hashSet);
//                Log.e("fileeee", "single nome do arquivo ： " + fileLists.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileLists;
    }


    //Nova interface, upload uma vez
//    private void uploadDoubleFIle(File ecgNew, File ppgNew) {
//
////        String userid = ACache.get(MyBleServiceFive.this).getAsString(MsgNum.AC_USER_ID);
////        String tokenVal = ACache.get(MyBleServiceFive.this).getAsString(MsgNum.AC_TOKEN_OLD);
//
//        int userid = (Integer) UserSPHelper.get(MyBleServiceFive.this, "userid", 0);
//        String tokenVal = (String) UserSPHelper.get(MyBleServiceFive.this, "token", "no");
//
//        HttpParams paramsPost = new HttpParams();
//        paramsPost.put("userId", userid);
//        paramsPost.put("filePpg200", ppgNew);
//        paramsPost.put("fileEcg200", ecgNew);
//        paramsPost.put("token", tokenVal);
//        paramsPost.put("version", "2");
//        //Carregar dados off-line ecg, PPG
//        paramsPost.put("uploadType", "off-line");
//        paramsPost.put("appVersion", "3.0");
//
//        OkGo.<String>post(Urls.BASE_URL_OLD + "/" + UrlPath.PATH_UPLOAD_PPG_FILE_URL).headers("Authorization", tokenVal)
//                .tag(MyBleServiceFive.this)
//                .params(paramsPost)
//                .isMultipart(true)
//                .execute(new StringCallback() {
//                    @Override
//                    public void onSuccess(Response<String> backresponse) {
//                        if (backresponse != null) {
//                            String backVal = backresponse.body();
//                            if (backVal != null) {
//                                Gson gson = new Gson();
//                                UploadResponse response = gson.fromJson(backVal, UploadResponse.class);
//                                if (response != null) {
//                                    if (response.getCode() == 200) {
////                                        Toast.makeText(MyBleService.this, "上传成功", Toast.LENGTH_SHORT).show();
//
//                                        deleteDoubleFileByTime(ecgNew, ppgNew);
//
//                                        //结束时，震动下
//                                    } else if (response.getStatus() == 1 && response.getMsg() != null && response.getMsg().equals("Erro de fósforos de token.")) {
//                                        Toast.makeText(MyBleServiceFive.this, response.getMsg(), Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        Toast.makeText(MyBleServiceFive.this, response.getMsg(), Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        super.onFinish();
//                    }
//                });
//    }



    private void save(final String fileNameVal,final int currentType,final byte[] historyPpgDatas) {

//        ppgTempDataContain.clear();
//        ppgTempDataContain.addAll(historyPpgDatas);
//        Log.e("dataRes2", "Salvar nome de arquivo PPG：" + fileNameVal + ":size=" + ppgTempDataContain.size());

        new Thread(new Runnable() {
            @Override
            public void run() {

                File file;

                if (currentType == 0) {
                    file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/double");
                } else {
                    file = new File(Environment.getExternalStorageDirectory(), "sungotwo2/single");
                }

                if (!file.exists()) {
                    file.mkdirs();
                }

                String ppgName = fileNameVal + ".ppg200";
                File ppgFile = new File(file, ppgName);
                try {

                    ppgFile.createNewFile();
                    FileOutputStream ppgFos = new FileOutputStream(ppgFile);

//                    for (Byte t : ppgTempDataContain) {
//                        ppgFos.write(t);
//                    }

                    ppgFos.write(historyPpgDatas);

                    ppgFos.flush();
                    ppgFos.close();

//                    ZqhLogUtils.e(TAG, ".... Salve um único ppg .....");

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    //Os dados são escritos locais, upload de chamadas
                    uploadDoubleFile();
                }
//                if (currentUpload == 0) {
//                    //0 representa uma sincronização de dados pareada,
//                    handleDoubleFileName(fileNameVal);
//                } else {
//                    //1 representa uma única sincronização de PPG
//                    handleSingleFileName(fileNameVal);
//                }

            }
        }).start();
    }

}
