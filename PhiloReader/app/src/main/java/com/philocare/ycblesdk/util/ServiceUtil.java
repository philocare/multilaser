/*
package com.philocare.ycblesdk.util;

import org.springframework.stereotype.Component;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


*/
/**
 * Filtro, processamento de solicitação de domínio cruzado
 *//*

@Component
public class RequestFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // Cabeçalho de resposta Especifica o caminho de URI que pode ser acessado acessado
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        //Cabeçalho de resposta Especifica um ou mais métodos permitidos para acessar os recursos para o tempo
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        //Defina o número máximo de segundos que possam sobreviver
        response.setHeader("Access-Control-Max-Age", "3600");
        //Configurar o cabeçalho de solicitação de suporte (solicitação de cabeçalho que pode ser acessado, por exemplo: token)
        response.setHeader("Access-Control-Allow-Headers", "*");
        //Resposta personalizada pode ser acessada
//        response.setHeader("Access-Control-Expose-Headers","checkTokenResult");
        // A resposta indicada pela solicitação indicada pode ser exposta a esta página.Pode ser exposto quando o valor verdadeiro é retornado
        response.setHeader("Access-Control-Allow-Credentials", "true");
        chain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}
*/
