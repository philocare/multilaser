package com.philocare.ycblesdk.util;

import android.text.format.Time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
    //Obtenha a data atual
    public static String currentDay() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }


    //Obtenha a data atual
    public static String currentDayDetail() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }


    //Obtenha a data atual
    public static String currentChatDayDetail() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    //Retorna yyyy-MM-dd hh:mm:ss
    public static String timeConvert(long timeData){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String date = sdf.format(new Date(timeData));
        return date;
    }

    //Retorna yyyy-MM-dd
    public static String timeConverDate(long timeData){
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String date2 = sdf2.format(new Date(timeData));
        return date2;
    }


    //Retorna yyyy-MM-dd
    public static String timeConverTimes(long timeData){
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date2 = sdf2.format(new Date(timeData));
        return date2;
    }


    //Retorna yyyy-MM-dd hh:mm:ss
    public static String hourTime(long timeData){

        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String dateString = formatter.format(currentTime);
        return dateString;
    }


    //Retorna yyyy-MM-dd
    public static String getCurrentDay(){
        Date currentTime = new Date();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String date2 = sdf2.format(currentTime);
        return date2;
    }

    public static Date StringToDate(String datetime){
        SimpleDateFormat sdFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            date = sdFormat.parse(datetime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }


    /**
     * Julgando se o tempo atual do sistema está dentro de um tempo específico
     *
     * @Param beginhour começa horas, por exemplo 5
     * @Param beginmin começa horas um número de minutos, por exemplo 00
     * @Param endhour horas finais, por exemplo 8
     * @Param endmin termina o número de minutos, por exemplo 00
     * @Return True é representado dentro do intervalo, caso contrário falso
     */
    public static boolean isCurrentInTimeScope(int beginHour, int beginMin, int endHour, int endMin) {
        boolean result = false;// 结果
        final long aDayInMillis = 1000 * 60 * 60 * 24;// Um dia todos os milissegundos
        final long currentTimeMillis = System.currentTimeMillis();// hora atual

        Time now = new Time();// Observe que quando introduzido aqui, escolha a classe Android.Text.Format.Time em vez da classe Java.sql.time
        now.set(currentTimeMillis);

        Time startTime = new Time();
        startTime.set(currentTimeMillis);
        startTime.hour = beginHour;
        startTime.minute = beginMin;

        Time endTime = new Time();
        endTime.set(currentTimeMillis);
        endTime.hour = endHour;
        endTime.minute = endMin;

        if (!startTime.before(endTime)) {
            // Circunstâncias especiais（por exemplo 22:00 - 8:00）
            startTime.set(startTime.toMillis(true) - aDayInMillis);
            result = !now.before(startTime) && !now.after(endTime); // startTime <= now <= endTime
            Time startTimeInThisDay = new Time();
            startTimeInThisDay.set(startTime.toMillis(true) + aDayInMillis);
            if (!now.before(startTimeInThisDay)) {
                result = true;
            }
        } else {
            // Situação ordinária(por exemplo 8:00 - 14:00)
            result = !now.before(startTime) && !now.after(endTime); // startTime <= now <= endTime
        }
        return result;
    }
}

