package com.example.teste_integracao;
import android.os.Bundle;
import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import android.os.Handler;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "com.example.teste_integracao";
  private EventChannel eventChannel;


  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
  super.configureFlutterEngine(flutterEngine);
    new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
        .setMethodCallHandler(
          (call, result) -> {
            if (call.method.equals("enviar")) {
                final String macBLE = call.argument("macBLE");

                result.success("Valor do MacBLE print pelo Java " + macBLE);
              }
          }
        );

        new EventChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), "events").setStreamHandler(
          new EventChannel.StreamHandler() {
              @Override
              public void onListen(Object args, EventChannel.EventSink events) {     
                startListening(args, events);
              }

              @Override
              public void onCancel(Object args) {                                
                cancelListening(args);
              }
          }
        );          
      }

  // Listeners
  private Map<Object, Runnable> listeners = new HashMap<>();

  void startListening(Object listener, EventChannel.EventSink emitter) {
    // Prepare a timer like self calling task
    final Handler handler = new Handler();
    listeners.put(listener, new Runnable() {
      @Override
      public void run() {
        if (listeners.containsKey(listener)) {
          // Send some value to callback
          emitter.success("Hello listener! " + (System.currentTimeMillis() / 1000));
          handler.postDelayed(this, 10000);
        }
      }
    });

    // Run task
    handler.postDelayed(listeners.get(listener), 1000);
  }

  void cancelListening(Object listener) {
    // Remove callback
    listeners.remove(listener);
    Log.d("cancelListening in Java", "Count: " + listeners.size());
  }
}