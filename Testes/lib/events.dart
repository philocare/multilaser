import 'package:flutter/services.dart';

const _channel = EventChannel('events');

typedef Listener = void Function(dynamic msg);
typedef CancelListening = void Function();

int nextListenerId = 1;

CancelListening startListening(Listener listener) {
  var subscription = _channel.receiveBroadcastStream(nextListenerId++).listen(listener, cancelOnError: true);

  return () {
    subscription.cancel();
  };
}
