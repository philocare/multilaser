package com.example.ycblesdkdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.yucheng.ycbtsdk.Constants;
import com.yucheng.ycbtsdk.YCBTClient;
import com.yucheng.ycbtsdk.response.BleDataResponse;
import com.yucheng.ycbtsdk.response.BleRealDataResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TimeSetActivity extends Activity {

    public static String Tag = TimeSetActivity.class.getName();


    private TextView startTestView;

    private TextView jianceModeSetView;
    private TextView setWenShiView;
    private TextView setHuangjingguangView;
    private TextView wenshiView;
    private TextView huanjingguangView;
    private TextView xueyangView;

    private TextView getXueyangValView;

    private TextView tuoluojiluView;

    private TextView addRCView;
    private TextView updateRCView;
    private TextView getRCView;
    private TextView delRCView;


    private TextView rockView;
    private TextView mobileTypeView;

    private TextView weatherView;
    private TextView callPhoneView;

    private TextView findPhoneView;

    private TextView richengSetView;

    private TextView yiwaiView;

    private TextView setLiangDuView;

    private TextView xipingView;

    private TextView taiwanLiangView;

    private TextView wuraoSetView;

    private TextView guanjiView;
    private TextView chognqiView;
    private TextView huifuchuchangView;

    private TextView setModeView;

    private TextView getModeView;

    private TextView setDataView;

    private TextView huanjingGuangSetView;

    private TextView wenshiduSetView;

    private TextView tiwenView;

    private TextView setGoalView;

    private TextView baseHrView;

    private TextView sportNoticeView;

    private TextView appSendMsgView;

    private TextView sportDataView;

    private TextView sleepDataView;

    private TextView tiwenNoticeView;

    private TextView setBiaoPanView;

    private TextView getBiaoPanView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);
        initView();
    }

    private void initView() {

        startTestView = (TextView) this.findViewById(R.id.time_start_test_view);

        setWenShiView = (TextView) this.findViewById(R.id.set_wenshi_view);
        setHuangjingguangView = (TextView) this.findViewById(R.id.set_huanjingguang_view);
        wenshiView = (TextView) this.findViewById(R.id.get_wenshi_view);
        huanjingguangView = (TextView) this.findViewById(R.id.get_huanjingguang_view);
        jianceModeSetView = (TextView) this.findViewById(R.id.set_jiance_view);
        xueyangView = (TextView) this.findViewById(R.id.get_xueyang_view);
        getXueyangValView = (TextView) this.findViewById(R.id.get_xueyangval_view);
        tuoluojiluView = (TextView) this.findViewById(R.id.get_tuoluojilu_view);

        addRCView = (TextView) this.findViewById(R.id.get_add_richeng_view);
        updateRCView = (TextView) this.findViewById(R.id.get_udpate_richeng_view);
        getRCView = (TextView) this.findViewById(R.id.get_get_richeng_view);
        delRCView = (TextView) this.findViewById(R.id.get_delete_richeng_view);

        rockView = (TextView) this.findViewById(R.id.get_qinyou_rock_view);

        mobileTypeView = (TextView) this.findViewById(R.id.get_send_mobile_type_view);

        weatherView = (TextView) this.findViewById(R.id.get_send_weather_view);

        callPhoneView = (TextView) this.findViewById(R.id.get_call_phone_view);

        findPhoneView = (TextView) this.findViewById(R.id.find_phone_view);

        richengSetView = (TextView) this.findViewById(R.id.set_richeng_order_view);

        yiwaiView = (TextView) this.findViewById(R.id.yiwai_jiance_view);


        setLiangDuView = (TextView) this.findViewById(R.id.liangdu_set_view);

        xipingView = (TextView) this.findViewById(R.id.xiping_set_view);

        taiwanLiangView = (TextView) this.findViewById(R.id.taiwan_liangping_view);

        wuraoSetView = (TextView) this.findViewById(R.id.wurao_set_view);

        guanjiView = (TextView) this.findViewById(R.id.guanji_view);
        chognqiView = (TextView) this.findViewById(R.id.reset_view);
        huifuchuchangView = (TextView) this.findViewById(R.id.huifuchuchang_view);

        setModeView = (TextView) this.findViewById(R.id.set_mode_view);
        getModeView = (TextView) this.findViewById(R.id.get_mode_view);
        setDataView = (TextView) this.findViewById(R.id.set_data_val_view);

        huanjingGuangSetView = (TextView) this.findViewById(R.id.huanjing_guang_set_view);
        wenshiduSetView = (TextView) this.findViewById(R.id.wenshidu_set_view);
        tiwenView = (TextView) this.findViewById(R.id.tiwen_set_view);
        setGoalView = (TextView) this.findViewById(R.id.set_sport_gorable);
        baseHrView = (TextView) this.findViewById(R.id.base_hr_data);
        sportNoticeView = (TextView) this.findViewById(R.id.sport_notice_view);
        appSendMsgView = (TextView) this.findViewById(R.id.app_send_msg_view);
        sportDataView = (TextView) this.findViewById(R.id.sport_data_view);

        sleepDataView = (TextView) this.findViewById(R.id.sleep_data_view);

        tiwenNoticeView = (TextView) this.findViewById(R.id.tiwen_data_view);

        setBiaoPanView = (TextView) this.findViewById(R.id.set_biaopan_view);

        getBiaoPanView = (TextView) this.findViewById(R.id.get_biaopan_view);

        //Inicie o teste de 90 segundos
        startTestView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOpenBloodOrder();
            }
        });


        //Ligue a temperatura e umidade, a luz ambiente, você precisa definir primeiro o modo de detecção
        //Temperatura e umidade, configurações de padrão
        setWenShiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingAmbientTemperatureAndHumidity(true, 10, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Defina a chamada de temperatura e umidade ...");
                    }
                });
            }
        });


        //Conjunto de luz ambiente
        setHuangjingguangView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingAmbientLight(true, 10, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Conjunto de chamada de luz ambiente ...");
                    }
                });
            }
        });


        wenshiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(0x051C, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Temperatura e umidade.........." + resultMap.toString());
                    }
                });
            }
        });

        huanjingguangView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(0x0520, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Luz ambiente.........." + resultMap.toString());
                    }
                });
            }
        });


        //Ligue o modo de detecção ou o modo de teste único
        jianceModeSetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appAmbientLightMeasurementControl(0x02, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Detectar o retorno de chamada do modo..........");
                    }
                });
            }
        });

        //Obter oxigênio do sangue
        xueyangView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingBloodOxygenModeMonitor(true, 10, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
//                        Log.e(Tag, "Defina a chamada de oxigênio no sangue.........."+resultMap.toString());
                        Log.e(Tag, "Defina a chamada de oxigênio no sangue..........code=" + code);
                    }
                });
            }
        });


        getXueyangValView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(0x051A, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
//                        Log.e(Tag, "Obtenha o retorno de retorno de oxigênio no sangue.........."+resultMap.toString());
                        if (resultMap != null) {
                            ArrayList lists = (ArrayList) resultMap.get("data");

                            Log.e(Tag, "Obtenha o retorno de retorno de oxigênio no sangue..........");
//                            for (HashMap map : lists) {
//
//                            }
                        } else {
                            Log.e(Tag, "O valor de oxigênio não está vazio..........");
                        }

                    }
                });
            }
        });


        tuoluojiluView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.healthHistoryData(0x0529, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Obter recordes de queda.........." + resultMap.toString());
                    }
                });
            }
        });


//        /**
// * Agendar configurações de modificação
// *
// * @param Type 0x00: Modificar programação 0x01: Adicionar programação 0x02: Excluir escala
// * @Param SchallyIndex Schedule Index 1-20 (adicione um índice mais 1, o máximo não pode exceder 20)
// * @param Scheduleenable 0x00: proibido 0x01: Ativar
// * @Param EventIndex modificou o índice de evento 1-20 (existem vários eventos em uma programação, o intervalo de índice é de 1 a 20, cada índice de evento não pode ser repetido) [Suporte vários eventos, a mesma instrução chamam várias vezes várias vezes, o Tipo de evento e tempo são diferentes, os seguintes parâmetros podem ser alterados]
// * @param eventenable 0x00: Desativar 0x01: Ativar
// * @param tempo modificando o formato de hora do evento é yyyy-mm-dd hh: mm: ss
// * @param eventType Tipo de evento 0x00 Get Up 0x01 Café da manhã 0x02 Sun 0x03 Almoço 0x04 Lunch Break 0x05 Sports 0x06 Jantar 0x07 Sleep 0x08 Personalizado
// * @param conteúdo Modifique o nome do tipo de evento (quando você deve ser personalizado, você pode passar o nome do evento (comprimento da string, não maior que 24Byte), caso contrário, null)
// * @param datearesponse
//         */
//        public static void settingScheduleModification(int type, int scheduleIndex, int scheduleEnable, int eventIndex, int eventEnable, String time, int eventType, String content, BleDataResponse dataResponse) {
        addRCView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingScheduleModification(0x01, 2, 0x01, 7, 0x01, "2020-08-29 17:18:00", 0x00, null, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {

                    }
                });

//                YCBTClient.settingScheduleModification(0x01, 1, 0x01, 2, 0x01, "2020-07-22 09:00:00", 0x01, null, new BleDataResponse() {
//                    @Override
//                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
//
//                    }
//                });
            }
        });


        //Quando você modifica, você só pode alterar o tipo de evento, o timestamp (index não pode mover, de outra forma)
        updateRCView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingScheduleModification(0x00, 3, 0x01, 2, 0x01, "2020-09-22 06:00:00", 0x00, null, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {

                    }
                });
            }
        });

        getRCView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent rcIntent = new Intent(TimeSetActivity.this, RCActivity.class);
                startActivity(rcIntent);

            }
        });


        //Ao excluir, passe dois índices
        delRCView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingScheduleModification(0x02, 2, 0x01, 1, 0x01, "2020-07-22 06:00:00", 0x00, null, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {

                    }
                });
            }
        });


        //Índice 0-4.
        rockView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appEmoticonIndex(4, 9, 40, "俺ÉDabao1", new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {

                    }
                });
            }
        });


        mobileTypeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appMobileModel("huawei40", new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e(Tag, "Callback de interação Bluetooth .....");
                        Log.e(Tag, resultMap.toString());
                    }
                });
            }
        });


        // número de clima numérico
        // 0-6 para a empresa manter as empresas reservadas
        // 7 dia ensolarado
        // 8 nublado.
        // 9 tempestade
        // 10 luz chuva
        // 11 chuva
        // 12 chuva forte
        // 13 chuva
        // 14 neve.
        // 15 na neve
        // 16 neve pesada
        // 17 flutuando
        // 18 névoa
        // 19.
        // 20 vento
        // 0 Desconhecido
        weatherView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appTodayWeather("20", "40", "32", 8, null, "4", null, 7, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {

                    }
                });
            }
        });


        //chamada recebida
        callPhoneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appSengMessageToDevice(0x00, "Preocupar", "1992325392", new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {

                    }
                });
            }
        });


        //Switch Switch
        richengSetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingScheduleSwitch(true, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });


        //Interruptor de monitoramento de acidentes
        yiwaiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingAccidentMode(true, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });


        //Conjunto de brilho da tela
        setLiangDuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingDisplayBrightness(0, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });

        //Configuração de tempo de tela de interesse
        xipingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingScreenTime(3, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });

        //Levante o pulso
        taiwanLiangView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingRaiseScreen(1, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });

        //Não perturbe o ambiente
        wuraoSetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingNotDisturb(1, 6, 30, 16, 30, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });


//        guanjiView = (TextView)this.findViewById(R.id.guanji_view);
//        chognqiView = (TextView)this.findViewById(R.id.reset_view);
//        huifuchuchangView = (TextView)this.findViewById(R.id.huifuchuchang_view);

        //Desligar
        guanjiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appShutDown(1, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });

        //Reiniciar
        chognqiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appShutDown(3, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });

        //Restaurar fábrica
        huifuchuchangView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingRestoreFactory(new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "code=" + code);
                    }
                });
            }
        });


        //Configuração do modo de trabalho 0x00: Definir para o modo Normal 0x01: Definir para CARE MODO DE TRABALHO 0X02: Definir para o modo de trabalho da sociedade de energia 0x03: Definir para o modo de trabalho personalizado
        setModeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingWorkingMode(3, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código do modo de trabalho=" + code);
                    }
                });
            }
        });


        //Obtenha o modo de trabalho atual do sistema
        getModeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.getCurrentSystemWorkingMode(new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Modo operacional" + resultMap.toString());
                    }
                });
            }
        });


        //Definir limiar de dados
        setDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingUploadReminder(true, 80, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código do limiar=" + code);
                    }
                });
            }
        });


        /***
         * Configuração de aquisição de dados
         * @Param em 0x01: aberto 0x00: fechar
         * @Param Tipo 0x00: PPG 0x01: Dados de Aceleração 0x02: ECG 0x03: Temperatura e umidade 0x04: Luz ambiente 0x05: temperatura corporal
         * @Param Collectlong cada vez que você coleta longa (unidade: segundo) (0)
         * @Param CollectInterVal Space de aquisição (unidade: minuto) (preencher desligado)
         * @Param dataresponse.
         */
        huanjingGuangSetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingDataCollect(1, 0x04, 90, 60, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código óptico ambiente=" + code);
                    }
                });
            }
        });

        /***
         * Configuração de aquisição de dados
         * @Param em 0x01: aberto 0x00: fechar
         * @Param Tipo 0x00: PPG 0x01: Dados de Aceleração 0x02: ECG 0x03: Temperatura e umidade 0x04: Luz ambiente 0x05: temperatura corporal
         * @Param Collectlong cada vez que você coleta longa (unidade: segundo) (0)
         * @Param CollectInterVal Space de aquisição (unidade: minuto) (preencher desligado)
         * @Param dataresponse.
         */
        wenshiduSetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingDataCollect(1, 0x03, 90, 60, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código de temperatura e umidade=" + code);
                    }
                });
            }
        });

        /***
         * Configuração de aquisição de dados
         * @Param em 0x01: aberto 0x00: fechar
         * @Param Tipo 0x00: PPG 0x01: Dados de Aceleração 0x02: ECG 0x03: Temperatura e umidade 0x04: Luz ambiente 0x05: temperatura corporal
         * @Param Collectlong cada vez que você coleta longa (unidade: segundo) (0)
         * @Param CollectInterVal Space de aquisição (unidade: minuto) (preencher desligado)
         * @Param dataresponse.
         */
        tiwenView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingDataCollect(1, 0x05, 90, 60, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código de temperatura corporal=" + code);
                    }
                });
            }
        });

        //Meta de movimento
        setGoalView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingGoal(0x00, 500, 0, 0, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código de destino=" + code);
                    }
                });
            }
        });


        baseHrView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appEffectiveHeart(80, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código da frequência cardíaca=" + code);
                    }
                });
            }
        });

        //Risco esportivo
        sportNoticeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appEarlyWarning(3, null, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Código de risco esportivo=" + code);
                    }
                });
            }
        });

        /**
         * Informações do aplicativo Push
         *
         * @Param Type Type 0x00 tem um novo relatório semanal, por favor, verifique no aplicativo.
         * 0x01 tem uma nova geração de livros mensal, por favor, vá para o aplicativo.
         * 0x02 Receba amigos e parentes, por favor, vá para o aplicativo.
         * 0x03 não mediu há muito tempo, medi-lo.
         * 0x04 Você tem consulta com sucesso.
         * 0x05 Sua consulta começará em uma hora.
         * 0x06 conteúdo personalizado
         * O conteúdo de informações da mensagem @param é 0x06, há apenas conteúdo de informações
         * @Param dataresponse 0x00: Sucesso de sincronização de pulseira 0x01: bracelete síncrono falhou
         */
        appSendMsgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appPushMessage(1, null, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Informações do aplicativo Push.=" + code);
                    }
                });
            }
        });

        /**
         * Número de etapa efetivo do aplicativo
         * Dados do esporte de volta
         * @Param passo de falha de passo
         * @Param tipo tipo de movimento 0x00: Sparading suitable 0x02: Casual Seminger 0x02: Cardiopulmonar Intensivo 0x03: Reduzir o Batom 0x04: Limite de movimento 0x05: Status vazio
         * @Param dataresponse 0x00: Sucesso de sincronização de pulseira 0x01: bracelete síncrono falhou
         */
        sportDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appEffectiveStep(10000, 1, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Sportswank.=" + code);
                    }
                });
            }
        });


/**
* App dados de sono de volta para a pulseira
 *
 * @Param deepsleeptimehour dorme profundamente (horas)
 * @Param deepsleeptimemin a dorme profundamente (minutos)
 * @Param lightsleeptimehour é raso (horas)
 * @Param lightsleeptimemin (minutos)
 * @Param totalsleeptimehour total de sono (horas)
 * @Param totalsleeptimemin total de sono (minuto)
 * @Param DataResponse 0x00: Bracelete Receber sucesso 0x01: Configuração Falha - Erro de parâmetros
 */
        sleepDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.appSleepWriteBack(3, 40, 2, 20, 6, 0, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Dormir de volta=" + code);
                    }
                });
            }
        });


        /**
         * Alarme de temperatura adicionou uma parte decimal, a nova pulseira tem essa função
         * Alarme de temperatura corporal
         * @Param on_off switch de alarme de temperatura
         * @Param MaxTemPinteger Alarme de Altura Integer Parte (36 - 100)
         * @Param mintempinteger Alarme de baixa temperatura de alarme parte inteira (-127 - 36)
         * @Param maxtempfloat alear a fração de alarme (1 - 9)
         * @Param mintempfloat fracção de alarme de baixa temperatura (1 - 9)
         * @Param dataresponse.
         */
        tiwenNoticeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.newSettingTemperatureAlarm(true, 37, 33, 3, 3, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Alarme de temperatura corporal=" + code);
                    }
                });
            }
        });


        //Definir mostrando
        setBiaoPanView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.settingMainTheme(1, new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Definir código de discagem.=" + code);
                    }
                });
            }
        });

        getBiaoPanView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YCBTClient.getThemeInfo(new BleDataResponse() {
                    @Override
                    public void onDataResponse(int code, float ratio, HashMap resultMap) {
                        Log.e("TimeSetActivity", "Obtenha o mostrador=" + resultMap.toString());
                    }
                });
            }
        });
    }


    private void isOpenBloodOrder() {
        Log.e("historyorder", "Teste em tempo real, clique no botão Abrir...");
        YCBTClient.appEcgTestStart(new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {

                try {
                    if (i == Constants.CODE.Code_OK) {
                        Log.e("historyorder", "Teste de inauguração do teste em tempo real=" + i);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                beginAnnation();
//                            }
//                        });

                    } else {
                        Log.e("historyorder", "Teste em tempo real falhou=" + i);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new BleRealDataResponse() {
            @Override
            public void onRealDataResponse(int dataType, HashMap hashMap) {
//                        Log.e("historytest", "Resultado de retorno de teste em tempo real=" + hashMap.toString());
                if (dataType == Constants.DATATYPE.Real_UploadHeart) {
                    //Dados da frequência cardíaca dataMap
                    try {
                        if (hashMap != null) {
                            String backVal = hashMap.toString();
                            JSONObject jsonObject = new JSONObject(backVal);
                            String hrVal = jsonObject.getString("heartValue");
                            Log.e("historyorhr", "Taxa cardíaca de teste em tempo real=" + hrVal);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (dataType == Constants.DATATYPE.Real_UploadBlood) {
                    //Dados da pressão arterial dataMap
                    try {
                        if (hashMap != null) {
                            String backVal = hashMap.toString();
                            JSONObject jsonObject = new JSONObject(backVal);
                            String lowVal = jsonObject.getString("bloodSBP");
                            String highVal = jsonObject.getString("bloodDBP");

                            Log.e("historyorhr", "highval=" + highVal);
                            Log.e("historyorhr", "lowval=" + lowVal);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (dataType == Constants.DATATYPE.Real_UploadPPG) {

                    Log.e("historyorhr", "Dados de PPG de teste em tempo real");

                    //Dados de PPG em tempo real  dataMap
                    byte[] ppgBytes = (byte[]) hashMap.get("data");
                    for (byte ecgbyte : ppgBytes) {
                    }
                } else if (dataType == Constants.DATATYPE.Real_UploadECG) {

//                    if (beginReceiveData == 0) {
//                        beginReceiveData = 1;
//                        //Ao iniciar o recebimento de dados, ligue a animação
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mHandlerTwo.removeMessages(MSG_CODE_TWO);
//                                openBloodTestSuccess();
//                            }
//                        });
//                    }
//                    Log.e("historyorhr", "Dados de ECG de teste em tempo real");
//
//                    //Dados de ECG em tempo real  dataMap
//                    byte[] ecgBytes = (byte[]) hashMap.get("data");
//                    for (byte ecgbyte : ecgBytes) {
//                        ecgVals.add(ecgbyte);
//                    }
                } else if (dataType == Constants.DATATYPE.AppECGPPGStatus) {
                    int EcgStatus = (int) hashMap.get("EcgStatus");
                    int PPGStatus = (int) hashMap.get("PPGStatus");
                    if (EcgStatus == 0) {
                        Log.e("status", "ECG é bom em contato com o ECG");
                    } else if (EcgStatus == 1) {
                        Log.e("status", "Eletrodos elétricos elétricos caem");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    } else if (EcgStatus == 2) {
                        Log.e("status", "Clique para cair");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }

                    if (PPGStatus == 0) {
                        Log.e("status", "Eletrocardiografia PPG é boa");
                    } else if (PPGStatus == 1) {
                        Log.e("status", "Clique para cair");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    } else if (PPGStatus == 2) {
                        Log.e("status", "Clique para cair");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }

                }
            }
        });

    }


}
