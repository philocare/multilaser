package com.example.ycblesdkdemo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.ycblesdkdemo.util.PermissionUtils;

import java.util.ArrayList;
import java.util.List;

public class WelComActivity extends Activity {


    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE
            , Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS, Manifest.permission.CALL_PHONE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Layout do welcome
        setContentView(R.layout.activity_welcome);

        // Verifica se a versão do SDK Android é M
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Verifica se tem todas as permissões e pede ao usuário a autorização
            boolean backBoolean = PermissionUtils.checkPermissionArray(WelComActivity.this, permissionArray, 3);
            if (backBoolean) {
                // ################################################################
                // Inicia o aplicativo
                afterDo();
            }
        } else {
            // ################################################################
            // Inicia o aplicativo
            afterDo();
        }

    }


    //Após a autorização, inicie
    private void afterDo() {
        // Aqui inicializa a tela principal "MainActivity"
        Intent mainIntent = new Intent(WelComActivity.this, MainActivity.class);
        startActivity(mainIntent);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode != 3) {
            return;
        }

        if (grantResults.length > 0) {
            List<String> deniedPermissionList = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    deniedPermissionList.add(permissions[i]);
                }
            }

            if (deniedPermissionList.isEmpty()) {
                //Todos licenciados
                afterDo();
            } else {

                //Se houver alguma permissão, continue consultas
                PermissionUtils.checkPermissionArray(WelComActivity.this, permissionArray, 3);
                //Verifique as opções na caixa de diálogo, retorne false
                for (String deniedPermission : deniedPermissionList) {
                    // Se houve permissões negadas, informar ao usuário o porque da necessidade da permissão
                    // No comando abaixo pode por uma Activity para ser executada caso a permissão esteja negada
                    //boolean flag = ActivityCompat.shouldShowRequestPermissionRationale(activityPermission, deniedPermission);
                    boolean flag = shouldShowRequestPermissionRationale(deniedPermission);
                    if (!flag) {
                        //Negar
                        Toast.makeText(WelComActivity.this, "Por favor, vá para a página de configurações, abra manualmente as permissões", Toast.LENGTH_SHORT).show();
//                        permissionShouldShowRationale(deniedPermissionList);
                        return;
                    }
                }
                //Negar
//                permissionHasDenied(deniedPermissionList);
            }
        }
    }

}
