package com.philocare.philoscan;

import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;

import io.flutter.plugin.common.EventChannel;

public class BcastReceiver extends BroadcastReceiver {

    private static final String BOOT_COMPLETED = Intent.ACTION_BOOT_COMPLETED;
    private static final String QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON";
    private static final String MY_PACKAGE_REPLACED = Intent.ACTION_MY_PACKAGE_REPLACED;

    private static final String TAG = BcastReceiver.class.getSimpleName();

    //private IRemoteService mService;
    private String pathLog = "/philocare/log/";
    private boolean bSave = true;
    private Context mCtx;
    private int sleepcount = 0;
    private boolean bStart = false;

    private static JobScheduler jobScheduler = null;

    public EventChannel.EventSink events;

    /**
     * Notificação broadcast
     */
    private NotificationActivity note;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        final String action = intent.getAction();

        Bundle extras = intent.getExtras();
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        if (status == BatteryManager.BATTERY_STATUS_UNKNOWN) {
            //events.error("UNAVAILABLE", "Charging status unavailable", null);
        } else {
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            //events.success(isCharging ? "charging" : "discharging");
        }




        assert action != null;
        mCtx = context;

        if (action.equals(Consts.DATA_SENT)) {
            try {
                Uri path = Uri.parse(String.valueOf(R.raw.ding));
                Ringtone notification = RingtoneManager.getRingtone(mCtx, path);
                notification.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (action.equals(Consts.DEVICE_CONNECTED)) {
            try {
                note = new NotificationActivity(mCtx, MainActivity.class);
                note.send_note(mCtx.getString(R.string.app_message_title),
                        mCtx.getString(R.string.app_message_title),
                        mCtx.getString(R.string.device_connected),
                        R.mipmap.ic_launcher_round);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (action.equals(Consts.SERVICE_START)) {
            try {
                note = new NotificationActivity(mCtx, MainActivity.class);
                note.send_note(mCtx.getString(R.string.app_message_title),
                        mCtx.getString(R.string.app_message_title),
                        mCtx.getString(R.string.local_service_started),
                        R.mipmap.ic_launcher_round);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (action.equals(Consts.DATA_COLLECTED)) {
            try {
                note = new NotificationActivity(mCtx, MainActivity.class);
                note.send_note(mCtx.getString(R.string.app_message_title), mCtx.getString(R.string.app_message_title),
                        mCtx.getString(R.string.app_message_extract),
                        R.mipmap.ic_launcher_round);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (action.equals(Consts.BATTERY)) {
            // pega o valor dos extras
//            assert extras != null;
//            MainActivity.batteryValue = extras.getByte(Consts.BATTERY_CHARGE, (byte) 0);
//            //MainActivity.updateProgressBar(value);
        }
        //if (action.equals(MainActivity.ACTION_SNOOZE)) {
        //    Toast.makeText(context, "Ação recebida de mensagem", LENGTH_LONG).show();
        //}
        //throw new UnsupportedOperationException("Not yet implemented");
    }

}