package com.philocare.philoscan;

/**
 * Created by Administrator on 2017/11/25.
 */

public interface Consts {

    String CHANNEL = "philocare.flutter.dev/sdk";
    String EVENT_CHANNEL = "philocare.flutter.dev/sdk_evt";
    int TIMEOUT_SECONDS = 25;
    String ALARM = "alarm";

    String BATTERY = "battery";

    String DATA_COLLECTED = "data_collected";
    String DATA_PART = "data_part";
    String BATTERY_CHARGE = "CHARGE";
    String DATA_SENT = "sent";
    String MESSAGE = "text";
    String MESSAGE_TEXT = "send_text";

    String LOG_FILE_PATH_NAME = "/philocare";
    String LOG_FILE_NAME_DATA_DATA = "log_file_data.txt";
    String LOG_FILE_NAME_DATA_POST = "log_file_post.txt";
    String LOG_FILE_NAME = "log_file.txt";

    String FILE_PATH_NAME = "/philocare";
    String FILE_NAME_DATA = "file_heath_data";
    String FILE_NAME_SLEEP_DATA = "file_sleep_data";

    String SERVICE_CHANNEL_ID = "service_philoscan";

    String RESURRECT_SERVICE = "LEVANTA-TI";
    boolean DEBUG = false;
    String ALARM_WAKE = "alarm_ring";
    String SERVICE_START = "service_start";
    String DEVICE_CONNECTED = "device_conn";
    int HIGH = 0x02;
    int PORTUGUESE = 0x08;
}
