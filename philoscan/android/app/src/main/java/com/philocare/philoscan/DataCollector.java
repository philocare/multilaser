package com.philocare.philoscan;


import android.content.Context;
import android.content.Intent;

import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.yucheng.ycbtsdk.YCBTClient;
import com.yucheng.ycbtsdk.response.BleConnectResponse;
import com.yucheng.ycbtsdk.response.BleDataResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DataCollector {

    private final static String TAG = MainActivity.class.getSimpleName();
    private final static String YOUR_APPLICATION = "E66_hub";

    private static Context mContext;
    // Código do dispositivo
    private static String mDeviceCode;

    // indica o estado de conexão com a pulseira
    private final boolean blConnected = false;

    // Tipo OAD ????
    private boolean mIsOadModel;

    private boolean isNewSportCalc = false;
    private byte batteryValue = 0;
    private int watchDataDay = 3;
    private int contactMsgLength = 0;
    private int allMsgLenght = 4;
    private boolean isSleepPrecision = false;
    private static boolean mConnFlag = false;
    private static boolean reset_date = false;

    // Variáveis relacionadas a bateria do dispositivo
    private static int batt_state = 0;
    public static int batt_value = 0;




    private String file_timestamp;

    private int count = 0;
    private int days_to_sync = 0;
    public int timer_counter=0;

    // 11/02/2022
    // Handler para atualizar a bateria
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            // Imagem
            if (batteryValue == 0) {

            }
            // Atualiza a imagem da bateria
            if (batteryValue == 1) {
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 2) {
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 3) {
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 4) {
            }

        }
    };

    //##########################################################################
    // Classes auxiliares


    // Construtor
    public DataCollector(Context ctx) {
        mContext = ctx;
    }

    // 16/02/2022 - Robson
    // Desconecta o dispositivo
    public void actionDisconnect() {
        mConnFlag = false;
    }

    // 16/02/2022 - Robson
    // Conecta o dispositivo
    public static void connectDevice(final String mac, final String deviceName) {

        mDeviceCode = mac;

        YCBTClient.connectBle(mac, new BleConnectResponse() {
            @Override
            public void onConnectResponse(int i) {
                sendBasicConfig();

                Intent intent = new Intent(mContext, BcastReceiver.class);
                intent.setAction(Consts.DEVICE_CONNECTED);
                mContext.sendBroadcast(intent);
            }
        });
    }

    public static int getDeviceBattery(Context ctx, Handler event_h, HashMap listeners, Object listener) throws InterruptedException {

        YCBTClient.getDeviceInfo(new BleDataResponse() {
            @Override
            public void onDataResponse(int code, float ratio, HashMap resultMap) {
                if (resultMap != null){

                    resultMap.keySet().forEach((key) -> {
                        Log.e("battery", key.toString());
                    });

                    // Valor das Chaves = [code, data, dataType]
                    // pegando o dado
                    //Log.e("hdata", "Valor da Chave code = " + hashMap.get("code"));
                    //Log.e("hdata", "Valor da Chave data = " + hashMap.get("data"));
                    //Log.e("hdata", "Valor da Chave dataType = " + hashMap.get("dataType"));

                    HashMap dataset = (HashMap) resultMap.get("data");

                    batt_value = (int) dataset.get("deviceBatteryValue");

                    //for (HashMap map: dataset) {
                    //dataset.keySet().forEach((key) -> {
                    //Log.e("battery", key.toString());
                    //});
                    Log.e("getbatt java", "Chama função post");
                    event_h.post((Runnable) listeners.get(listener));
                    Log.e("getbatt java", "Chamou...");

                    Log.e("battery", "Carga bateria evento: " + String.valueOf(batt_value) + "%");
                    //System.out.println(key);
                    //batt_state = (int) resultMap.get("deviceBatteryState");// if (blood_oxygen == 0)  no value
                    //batt_value = (int) resultMap.get("deviceBatteryValue");
                    //ctx.notify();
                }
            }
        });

        //ctx.wait();
        return batt_value;
    }

    // 16/02/2022 - Robson
    public static String getMac() {
        // Verifica se já tem alguma salva
        String dev = SysUtils.getSavedDevice(mContext);
        if (!dev.equals("")) {
            String[] devs = dev.split(";");
            return devs[1];
        } else {
            return "NONE";
        }
    }

    /**
     * Envia as configurações básicas para o dispositivo
     */
    public static void sendBasicConfig() {

        // Configura linguagem da pulseira para português
        YCBTClient.settingLanguage(0x08, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "Setando linguagem português");
            }
        });

        //Coleta de freqüência cardíaca, medir de minuto em minuto
        YCBTClient.settingHeartMonitor(0x01, 3, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "Definindo intervalo de 3 minutos para coleta da frequência cardíaca");
            }
        });

        // Luminosidade do display
        YCBTClient.settingDisplayBrightness(Consts.HIGH, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // Ok!!
                Log.e("device", "Setando brilho máximo");
            }
        });

        // Girar o pulso para ver
        YCBTClient.settingRaiseScreen(1, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // Ok!
                Log.e("device", "Girar o pulso para acender");
            }
        });

        // Pulso de utilização
        YCBTClient.settingHandWear(0, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // Ok
                Log.e("device", "Uso no braço esquerdo");
            }
        });

        // Não perturbar
        YCBTClient.settingNotDisturb(1, 23, 00, 7, 30, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // Ok
                Log.e("device", "Não perturbe das 23:00 até 07:30");
            }
        });

        // cor da pele
        YCBTClient.settingSkin(1, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // Ok
                Log.e("device", "Cor da pele branca nivel 1");
            }
        });

        // Monitoramento de SPO2
        YCBTClient.settingBloodOxygenModeMonitor(true, 3, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "Monitoramento de oxigênio no sangue de 3 em 3 minutos");
            }
        });

        YCBTClient.settingUnit(0, 0, 0, 0, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // Ok
                Log.e("device", "Unidades km, kg, °C, 24 hrs atribuidas");
            }
        });

        /***
         * Unit setting 0)
         * @param distanceUnit 0x00:km 0x01:mile
         * @param weightUnit  0x00:kg 0x01:lb 0x02:st
         * @param temperatureUnit  0x00: °C 0x01: °F
         * @param timeFormat  0x00:24hour0x01:12hour
         * @param dataResponse
         *
         * public static void settingUnit(int distanceUnit, int weightUnit, int temperatureUnit, int timeFormat, BleDataResponse dataResponse)
         *
         */

        // Ajusta a hora da smartband
        // YCBTClient.settingTime - comando sumiu da API mas está documentado

        //YCBTClient.collectHistoryDataWithTimestamp();
        // Grvar as informações de Usuário
        // public static void settingSleepRemind(int startHour, int startMin, int repeat, BleDataResponse dataResponse)
        // public static void settingHeartAlarm(int on, int highHeart, int lowHeart, BleDataResponse dataResponse)
        // public static void settingUserInfo(int height, int weight, int sex, int age, BleDataResponse dataResponse)
        // public static void settingHandWear(int leftOrRight, BleDataResponse dataResponse)
        // public static void settingSkin(int skinColor, BleDataResponse dataResponse)
        // public static void settingRaiseScreen(int on, BleDataResponse dataResponse)
        // public static void settingNotDisturb(int on,
        //                                         int startTimeHour, int startTimeMin,
        //                                         int endTimeHour, int endTimeMin, BleDataResponse dataResponse)
        //public static void settingLongsite(int time1StartHour, int time1StartMin, int time1EndHour, int time1EndMin,
        //                                       int time2StartHour, int time2StartMin, int time2EndHour, int time2EndMin,
        //                                       int intervalTime, int repeat, BleDataResponse dataResponse)
        // public static void settingGoal(int goalType, int target, int sleepHour, int sleepMinute, BleDataResponse dataResponse)
        // public static void settingDisplayBrightness(int level, BleDataResponse dataResponse)
        // YCBTClient.settingTemperatureAlarm
        // YCBTClient.settingTemperatureMonitor();
        // YCBTClient.settingBloodOxygenModeMonitor(); isEnable: If it is set to YES, it will be on; if it is set to NO, it will be turned off. timeInterval: monitoring interval, unit: minute, 1-60.
        // YCBTClient.settingScheduleModification(); ???
        // scheduleIndex:(NSInteger)scheduleIndex
        // eventType:(NSInteger)eventType
        // eventIndex:(NSInteger)eventIndex
        // eventEnable:(BOOL)eventEnable
        // eventTime:(NSInteger)eventTime
        // eventName:(NSString *)eventName
        // callback:(void (^)(Error_Code code,
        // eventType Description
        // 0x00 get up
        // 0x01 breakfast
        // 0x02 bask in the sunshine
        // 0x03 lunch
        // 0x04 noon break
        // 0x05 sport
        // 0x06 dinner
        // 0x07 sleep
        // 0x08 custom



       /* //Nenhuma detecção de sentido
        YCBTClient.settingPpgCollect(0x01, 60, 60, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                Log.e("device", "Definir nenhuma aquisição de dados");
            }
        });
        //Frequência cardíaca síncrona
        syncHisHr();
        //Sono síncrono
        syncHisSleep();

        syncHisStep();*/
    }

    // ############################################################################
    // Funções relacionadas ao SDK

    // ##################################################################################
    // 07/02/2022 - Robson
    // Coleta de dados da pulseira
    // dataResponse (Return data of parameter 0x0509):
    // 'startTime' - test time,
    // 'stepValue' - number of steps,
    // 'heartValue' - heart rate value,
    // 'DBPValue' - systolic blood pressure,
    // 'SBPValue' - diastolic blood pressure,
    // 'OOValue' - blood oxygen,
    // 'respiratoryRateValue' - respiratory rate,
    // 'hrvValue' - hrv,
    // 'cvrrValue' - cvrr,
    // 'tempIntValue' - integer part of temperature,
    // 'tempFloatValue' - decimal part of temperature
    //
    //Taxa cardíaca histórica síncrona
    public static void syncHealthData(Context ctx) {
        //YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistoryHeart, new BleDataResponse() {
        YCBTClient.healthHistoryData(0x0509, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {

                if (hashMap != null) {

                    String json_str = "";

                    //nregs++;
                    //Log.e("history", "hashmap size: " + hashMap.get("heartValue").toString());
                    // Valor das Chaves = [code, data, dataType]
                    // pegando o dado

                    //Log.e("hdata", "Valor da Chave code = " + hashMap.get("code"));
                    //Log.e("hdata", "Valor da Chave data = " + hashMap.get("data"));
                    //Log.e("hdata", "Valor da Chave dataType = " + hashMap.get("dataType"));

                    List<HashMap> dataset = (List<HashMap>) hashMap.get("data");
                    // Instante para geração do arquivo com nome diferenciado
                    Date now = new Date();

                    for (HashMap map: dataset) {

                        // pegamos todas as gradezas
                        int blood_oxygen = (int) map.get("OOValue");// if (blood_oxygen == 0)  no value
                        int tempIntValue = (int) map.get("tempIntValue");//Temp int value
                        int tempFloatValue = (int) map.get("tempFloatValue");//if (tempFloatValue == 15) the result is error
                        int hrv = (int) map.get("hrvValue");// if (hrv == 0)  no value
                        int cvrr = (int) map.get("cvrrValue");//  if (cvrr == 0)  no value
                        int respiratoryRateValue = (int) map.get("respiratoryRateValue");// if (respiratoryRateValue == 0)  no value
                        int steps = (int) map.get("stepValue");
                        long startTime = (long) map.get("startTime");
                        double temp = 0;
                        if (tempFloatValue != 15) {
                            temp = Double.parseDouble(tempIntValue + "." + tempFloatValue);
                        }

                        //Date date = new Date((Long) map.get("heartStartTime"));
                        Date date = new Date(startTime);
                        // format of the date
                        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        jdf.setTimeZone(TimeZone.getTimeZone("GMT-3"));
                        String java_date = jdf.format(date);

                        // Codificar uma string JSON
                        json_str = json_str + "{\"mac\":\"" + mDeviceCode +"\", \"type\":\"HEALTH\"," +
                                "\"timestamp\":\"" + java_date + "\"," +
                                "\"hr\":\"" + map.get("heartValue") + "\"," +
                                "\"spo2\":\"" + blood_oxygen + "\"," +
                                "\"hrv\":\"" + hrv + "\"," +
                                "\"cvrr\":\"" + cvrr + "\"," +
                                "\"respiratory\":\"" + respiratoryRateValue + "\"," +
                                "\"steps\":\"" + steps + "\"," +
                                "\"temp\":\"" + temp + "\"},\n";


//                        SysUtils.saveData(ctx, json_str, Consts.FILE_NAME_DATA + now.getTime() + ".txt");

                        //Log.e("hdata", json_str);
//                        Log.e("hdata", java_date);
//                        Log.e("hdata", String.valueOf(map.get("heartValue")));
//                        Log.e("hdata", String.valueOf(blood_oxygen));
//                        Log.e("hdata", String.valueOf(hrv));
//                        Log.e("hdata", String.valueOf(cvrr));
//                        Log.e("hdata", String.valueOf(respiratoryRateValue));
//                        Log.e("hdata", String.valueOf(steps));
//                        Log.e("hdata", String.valueOf(temp));
//                    Log.e("hdata", String.valueOf(dataset.get(463)));
//                    Log.e("hdata", String.valueOf(dataset.get(464)));
//                    Log.e("hdata", String.valueOf(dataset.get(465)));
                    }
                    // Remove o ultimo ;
                    json_str = json_str.substring(0, json_str.length() - 2);

                    SysUtils.saveData(ctx, "[" + json_str + "]", Consts.FILE_NAME_DATA + now.getTime() + ".txt");


                    //Log.e("history", "hashMap=" + hashMap.toString());

                    //Log.e("history", "hr time=" + hashMap.get("heartStartTime"));

                    //Log.e("history", "hr val=" + hashMap.get("heartValue"));

                } else {
                    Log.e("history", "no ..hr..data....");
                }
                //Log.e("history HR", "Número de registros: " + nregs);

//                syncHisStep();
            }
        });
    }

    // ##################################################################################
    // 08/02/2022 - Robson
    // Coleta de dados de sono da pulseira
    // 'startTime' - sleep start time
    // 'endTime' - sleep end time
    // 'deepSleepCount' - number of deep sleeps
    // 'lightSleepCount' - number of light sleeps
    // 'deepSleepTotal' - total time of deep sleep in minutes
    // 'lightSleepTotal'- -Total light sleep time in minutes
    // 'sleepData'-detailed sleep data set
    // 'sleepType': 0xF1: deep sleep 0xF2: light sleep
    // 'sleepStartTime'-start timestamp
    // 'sleepLen'-sleep duration in seconds
    //
    //
    public static void syncSleepData(Context ctx) {
        //YCBTClient.healthHistoryData(Constants.DATATYPE.Health_HistoryHeart, new BleDataResponse() {
        YCBTClient.healthHistoryData(0x0504, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {

                if (hashMap != null) {

                    String json_str = "";

                    List<HashMap> dataset = (List<HashMap>) hashMap.get("data");

                    long start_time = 0;
                    long end_time = 0;
                    int deep_sleep_count = 0;
                    int light_sleep_count = 0;
                    int deep_sleep_total = 0;
                    int light_sleep_total = 0;

                    // Instante para geração do arquivo com nome diferenciado
                    Date now = new Date();

                    for (HashMap map: dataset) {
                        // pegamos todas as gradezas
                        try {
                            start_time = (long) map.get("startTime");
                            end_time = (long) map.get("endTime");
                            deep_sleep_count = (int) map.get("deepSleepCount");
                            light_sleep_count = (int) map.get("lightSleepCount");
                            deep_sleep_total = (int) map.get("deepSleepTotal");
                            light_sleep_total = (int) map.get("lightSleepTotal");
                        } catch (Exception e) {
                            Log.e("healhistorydata", e.getMessage());
                        }

                        //Log.e("sleep", String.valueOf(map.get("sleepData")));
                        List<HashMap> sleep_dataset = (List<HashMap>) map.get("sleepData");

                        StringBuilder json_sleep_detais = new StringBuilder();
                        json_sleep_detais.append("\"details\": [");

                        for (HashMap sleep_map: sleep_dataset) {
                            // lista a construção do sono
                            int sleep_type = (int) sleep_map.get("sleepType");
                            long sleep_start_time = (long) sleep_map.get("sleepStartTime");
                            int sleep_len_sec = (int) sleep_map.get("sleepLen");
                            //Date date = new Date((Long) map.get("heartStartTime"));
                            Date date_sst = new Date(sleep_start_time);
                            // format of the date
                            SimpleDateFormat jdfsst = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            jdfsst.setTimeZone(TimeZone.getTimeZone("GMT-3"));
                            String date_st_str = jdfsst.format(date_sst);

                            json_sleep_detais.append(
                                    "{\"sleepStartTime\":\"").append(date_st_str).append("\",").
                                    append("\"sleepLen\":\"").append(sleep_len_sec).append("\",").
                                    append("\"sleepType\":\"").append(sleep_type).append("\"},");
                        }
                        json_sleep_detais.deleteCharAt(json_sleep_detais.length() - 1);
                        json_sleep_detais.append("]");


                        //Date date = new Date((Long) map.get("heartStartTime"));
                        Date date_st = new Date(start_time);
                        // format of the date
                        SimpleDateFormat jdfst = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        jdfst.setTimeZone(TimeZone.getTimeZone("GMT-3"));
                        String date_st_str = jdfst.format(date_st);

                        Date date_et = new Date(end_time);
                        // format of the date
                        SimpleDateFormat jdfet = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        jdfet.setTimeZone(TimeZone.getTimeZone("GMT-3"));
                        String date_et_str = jdfet.format(date_et);

//                        Date date_sst = new Date(sleep_start_time);
//                        // format of the date
//                        SimpleDateFormat jdfsst = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                        jdfsst.setTimeZone(TimeZone.getTimeZone("GMT-3"));
//                        String date_sst_str = jdfsst.format(date_et);


                        // Codificar uma string JSON
                        json_str = json_str + "{\"mac\":\"" + mDeviceCode +"\", \"type\":\"SLEEP\"," +
                                "\"startTime\":\"" + date_st_str + "\"," +
                                "\"endTime\":\"" + date_et_str + "\"," +
                                "\"deepSleepCount\":\"" + deep_sleep_count + "\"," +
                                "\"light_sleep_count\":\"" + light_sleep_count + "\"," +
                                "\"deepSleepTotal\":\"" + deep_sleep_total + "\"," +
                                "\"lightSleepTotal\":\"" + light_sleep_total + "\"" +
                                //"\"sleepData\":\"" + String.valueOf(sleep_data) + "\"," +
                                //"\"sleepType\":\"" + String.valueOf(sleep_type) + "\"," +
                                //"\"sleepStartTime\":\"" + date_sst_str + "\"," +
                                //"\"sleepLen\":\"" + String.valueOf(sleep_len_sec) +
                                ", " + json_sleep_detais + "}, \n";

                    }
                    // Remove o ultimo ;
                    json_str = json_str.substring(0, json_str.length() - 3);

                    SysUtils.saveData(ctx, "[" + json_str + "]", Consts.FILE_NAME_SLEEP_DATA +
                            now.getTime() + ".txt");

                    //Log.e("history", "hashMap=" + hashMap.toString());

                    //Log.e("history", "hr time=" + hashMap.get("heartStartTime"));

                    //Log.e("history", "hr val=" + hashMap.get("heartValue"));

                } else {
                    Log.e("sleep history", "no sleep data");
                }
                //Log.e("history HR", "Número de registros: " + nregs);

//                syncHisStep();
            }
        });
    }

    // Envia mensagem para a pulseira
    public static void sendMessage(String msg) {
        YCBTClient.appSengMessageToDevice(3, "REMÉDIO", msg, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                // OK
                if (i == 0) {
                    Log.e("mensage", "Mensagem enviada");
                } else {
                    Log.e("mensage", "Mensagem não enviada");
                }
            }
        });
    }

}
