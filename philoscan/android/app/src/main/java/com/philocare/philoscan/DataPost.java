package com.philocare.philoscan;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class DataPost {

    private Context mContext;

    // Construtor
    public DataPost(Context ctx) {
        mContext = ctx;
        //logger("Envia dados para o servidor: OK", "create");
    }

    /**
     * Envia dados para a servidor na nuvem
     */
    public void postAndGetJsonWebToken() throws InterruptedException {
        //HttpURLConnection conn = null;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {

                // executa três vezes a thread
                for (int i = 0; i < 1; i++) {

                    try {
                        //Your code goes here
                        String response = "";

                        // Dados para autenticação
                        StringBuilder sbParams = new StringBuilder();
                        sbParams.append("{");
                        sbParams.append("\"passwd\"");
                        sbParams.append(":");
                        sbParams.append("\"123456aA\"");
                        sbParams.append(",");
                        sbParams.append("\"email\"");
                        sbParams.append(":");
                        sbParams.append("\"philo\"");
                        sbParams.append("}@");

                        // Dados extraidos da pulseira
                        // Lê um arquivo e envia no post
                        String[] samples = SysUtils.readTxtFile(mContext);

                        if (samples == null) {
                            SysUtils.logger(mContext, "Sem dados para enviar.", "send json");
                            continue;
                        }

                        sbParams.append(samples[1]);
                        if (Consts.DEBUG)
                            SysUtils.logger(mContext, "Enviando arquivo: " + samples[0], "send");

                        // messagem a ser enviada
                        //logger("JSON: " + sbParams.toString(), "send json");


                        try {
                            //String requestURL = "https://www.philocare.com/engine/devicedatareceive_post.php";
                            String requestURL = "https://philo.solutions/engine/E66_devicedatareceive_post.php";

                            URL url;

                            try {
                                url = new URL(requestURL);

                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.setReadTimeout(15000);
                                conn.setConnectTimeout(15000);
                                conn.setRequestMethod("POST");
                                conn.setDoInput(true);
                                conn.setDoOutput(true);


                                OutputStream os = conn.getOutputStream();
                                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(os, "UTF-8"));
                                writer.write(sbParams.toString());
                                writer.flush();
                                writer.close();
                                os.close();
                                int responseCode = conn.getResponseCode();

                                if (responseCode == HttpsURLConnection.HTTP_OK) {
                                    String line;
                                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                    while ((line = br.readLine()) != null) {
                                        response += line;
                                    }
                                } else {
                                    response = "";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            //return response;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String millisInString = dateFormat.format(new Date());

                        Log.e("response", response.toString());

                        //logger(millisInString + ": " + response, "send json");
                        // apaga o arquivo enviado
                        if (!response.contains("not")) {
                            SysUtils.logger(mContext, "Dados recebidos e processados pelo servidor", "send json");
                            // #################################
                            SysUtils.deleteFile(mContext, samples[0]);
                            if (Consts.DEBUG)
                                SysUtils.logger(mContext, "Apagando arquivo: " + samples[0], "send");
                        } else {
                            SysUtils.logger(mContext, "Arquivo: " + samples[0] + " NÃO pode ser processado.", "send");
                            //SysUtils.moveFile(samples[0]);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
        //thread.join();
        //thread.start();

        //##############################################
//        Intent intent = new Intent(mContext, BcastReceiver.class);
//        intent.setAction(Consts.DATA_SENT);
        //sendBroadcast(intent);

    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
