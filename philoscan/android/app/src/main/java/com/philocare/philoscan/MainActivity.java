package com.philocare.philoscan;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodChannel;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.yucheng.ycbtsdk.YCBTClient;

import com.yucheng.ycbtsdk.response.BleDataResponse;


import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends FlutterActivity {
    private BroadcastReceiver broadcastReceiver = new BcastReceiver();

    //private final boolean flgTemp = false;
    private float valTemp = 0;
    private Context mContext;
    private DataCollector MainDataColector;
    private DataPost MainDataPost;

    final Handler event_handler = new Handler();
    // Listeners
    private Map<Object, Runnable> listeners = new HashMap<>();

    /**
     * Notificação broadcast
     */
    private NotificationActivity note;

    private final String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS, Manifest.permission.CALL_PHONE,
            Manifest.permission.RECEIVE_BOOT_COMPLETED
    };

    public final String[] macAddr = {"Nada ainda"};
    private Object event_listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = MainActivity.this;
        Log.e("oncreate", "Criação da interface principal do serviço");
        SysUtils.logger(mContext, "#########################################", "########");
        SysUtils.logger(mContext, "Criação da interface principal do serviço", "oncreate");

        // #####################################################
        // 14/02/2022 - Robson
        // Canal de mensagens na bandeja do Android
        createNotificationChannel();

        //######################################################
        // 16/02/2022 - Robson
        // Objeto de conexão e coleta de dados
        MainDataColector = new DataCollector(this);
        MainDataPost = new DataPost(this);

        // Verifica se tem todas as permissões e pede ao usuário a autorização
        boolean backBoolean = PermissionUtils.checkPermissionArray(this, permissionArray, 3);

        if (backBoolean) {
            Log.e("oncreate", "Permissões OK");
            SysUtils.logger(mContext, "Permissões Ok", "oncreate");
        } else {
            Log.e("oncreate", "Permissões com falha");
            SysUtils.logger(mContext, "Permissões com falha", "oncreate");
        }

        YCBTClient.initClient(getApplicationContext(), true);

        // 16/02/2022 - Robson
        // Esse exemplo de broadcast está funcionando e depende do broadcast que depende da
        // NotificationActivity
        //        Intent intent = new Intent(mContext, BcastReceiver.class);
        //        intent.setAction(Consts.SERVICE_START);
        //        mContext.sendBroadcast(intent);

        // verifica se tem algum dispositivo cadastrado
        String sv_device = SysUtils.getSavedDevice(this);
        String[] sv_dev_parts = sv_device.split(";");

        if (sv_dev_parts[0].equals("E66")) {
            Log.e("oncreate", "Tentando iniciar o serviço");
            // verifica se o serviço está rodando
        } else {
            SysUtils.logger(mContext, "Não existe dispositivo cadastrado.", "oncreate");
        }
    }



    void startListening(Object listener, EventChannel.EventSink emitter) {
        // Prepare a timer like self calling task
        listeners.put(listener, new Runnable() {
            @Override
            public void run() {
                if (listeners.containsKey(listener)) {
                    // Send some value to callback
                    emitter.success("Valor bateria do dispositivo: " + MainDataColector.batt_value);
                    //handler.postDelayed(this, 10000);
                }
            }
        });
        // Run task
        //listeners.get(listener);
        event_listener = listener;

        //event_handler.post(listeners.get(event_listener));
    }

    void cancelListening(Object listener) {
        // Remove callback
        listeners.remove(listener);
        Log.d("cancelListening in Java", "Count: " + listeners.size());
    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        new EventChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), Consts.EVENT_CHANNEL).setStreamHandler(
                new EventChannel.StreamHandler() {
                    @Override
                    public void onListen(Object args, EventChannel.EventSink events) {
                        startListening(args, events);
                    }

                    @Override
                    public void onCancel(Object args) {
                        cancelListening(args);
                    }
                }
        );


        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), Consts.CHANNEL)
                .setMethodCallHandler(
                        (call, result) -> {
                            // Lâ bateria do celular
                            if (call.method.equals("getBatteryLevel")) {

                                int batteryLevel = getBatteryLevel();
//                                String res = "";
//
//                                listeners.keySet().forEach((key) -> {
//                                    //res.concat(key.toString());
//                                });
                                //Log.e("getBatt", String.valueOf(listeners.keySet()));
                                result.success("Lendo a bateria...");
//
//                                if (batteryLevel != -1) {
//                                    result.success(batteryLevel);
//                                } else {
//                                    result.error("UNAVAILABLE", "Battery level not available.", null);
//                                }
                            }

                            // Lê a bateria do dispositivo
                            if (call.method.equals("getDeviceBatteryLevel")) {
                                int devicebatteryLevel = 0;
                                try {
                                    DataCollector.getDeviceBattery(this, event_handler, (HashMap) listeners, event_listener);

                                    result.success("Lendo a bateria da pulseira...");

                                    //Log.e("flutter engine", "Carga bateria: " + String.valueOf(devicebatteryLevel) + "%");

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                if (devicebatteryLevel != -1) {
                                    result.success(devicebatteryLevel);
                                } else {
                                    result.error("UNAVAILABLE", "Device battery level not available.", null);
                                }
                            }

                            // Inicia a varredura de pulseiras compatíveis
                            if (call.method.equals("startscan")) {
                                // Verifica se já tem alguma salva
                                String dev = SysUtils.getSavedDevice(MainActivity.this);

                                if (dev.equals("")) {
//                                    YCBTClient.startScanBle(new BleScanResponse() {
                                    YCBTClient.startScanBle((i, scanDeviceBean) -> {
                                        // Procura os dispositivos próximos
                                        if (scanDeviceBean != null) {
                                            // A cada dispositivo encontrado ele adiciona a lista se já não tiver
                                            // adicionado
                                            // O MAC da pulseira é adicionado
                                            //macAddr[0] = "Achou alguma coisa";
                                            macAddr[0] = scanDeviceBean.getDeviceMac();

                                            try {
                                                SysUtils.saveDevice(MainActivity.this, "E66", macAddr[0]);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            //result.success(macAddr[0]);
                                            Log.e("device", "mac=" + scanDeviceBean.getDeviceMac() + ";name=" + scanDeviceBean.getDeviceName() + "rssi=" + scanDeviceBean.getDeviceRssi());
                                        }
                                    }, Consts.TIMEOUT_SECONDS);
                                } else {
                                    String[] devs = dev.split(";");
                                    macAddr[0] = devs[1];

                                    Log.e("device", "mac=" + macAddr[0] + ";name=" + devs[0]);
                                    // Faltou permissão !!!!!!!!!!!!!!!!!!!!
                                    SysUtils.logger(MainActivity.this, "MAC lido do arquivo: "
                                            + macAddr[0],"startscan");

                                    result.success(macAddr[0]);

                                }
                            }

                            // Lô o mac da pulseira gravado em disco
                            if (call.method.equals("getmac")) {
                                String getMacAddr = DataCollector.getMac();
                                if (!getMacAddr.equals("")) {
                                    result.success(getMacAddr);
                                } else {
                                    result.error("UNAVAILABLE", "Pulseira ainda não encontrada", null);
                                }

                            }

                            // Apaga o mac da pulseira gravado em disco
                            if (call.method.equals("deletemac")) {
                                boolean res = SysUtils.deleteSavedDevice(MainActivity.this);
                                if (res) {
                                    result.success(true);
                                } else {
                                    result.error("ERRO", "Dispositivo não foi apagado", null);
                                }


                            }

                            // Conecta com a pulseira do mac passado e envia as configurações básicas
                            if (call.method.equals("connect")) {
                                String getMacAddr = DataCollector.getMac();

                                Log.e("device", "mac=" + getMacAddr);

                                // Solicita o driver para conectar na pulseira
                                DataCollector.connectDevice(getMacAddr, "E66");
                                result.success("Solicitado para pulseira " + getMacAddr);
                            }

                            // Busca os dados de saúde da pulseira conectada e grava em disco
                            if (call.method.equals("gethr")) {
                                // Ainda não implementado
                                DataCollector.syncHealthData(this);
                                //result.error("UNAVAILABLE", "Não implementado", null);
                            }

                            // Busca os dados de sono da pulseira conectada e grava em disco
                            if (call.method.equals("getsleep")) {
                                // Ainda não implementado
                                DataCollector.syncSleepData(this);
                                //result.error("UNAVAILABLE", "Não implementado", null);
                            }

                            // Envia uma mensagem para a pulseira conectada (não funcionou!!!)
                            if (call.method.equals("sendmsg")) {
                                final String msg = call.argument("message");
                                DataCollector.sendMessage(msg);
                            }

                            // Envia dados armazenados em disco para o nuvem
                            if (call.method.equals("sendpost")) {
                                try {
                                    MainDataPost.postAndGetJsonWebToken();
                                    result.success("Enviado para a nuvem");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                );
    }




    /**
     * After opening, the bracelet will be black. This is normal.
     */
    private void openRealTemp() {
        YCBTClient.appTemperatureMeasure(0x01, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (i == 0) {
                    //success
                    Log.e("device", "Temp open");

                    readRealTemp();
                }
            }
        });
    }

    /**
     * if your need more, you can loop through this method.
     * but it must be after start_real_temp method.
     */
    private void readRealTemp() {
        YCBTClient.getRealTemp(new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (i == 0) {
                    String temp = (String) hashMap.get("tempValue");

//                    Toast.makeText(MainActivity.this,
//                            "Temperatura lida: " + temp, Toast.LENGTH_LONG).show();
                    Log.e("device", "Temp read: " + temp);
                    if (temp != null) {
                        valTemp = Float.parseFloat(temp);
                    } else {
                        valTemp = -1;
                    }
                    closeRealTemp();
                }
            }
        });
    }

    /**
     * If you call the startRealTemp() method, you must call this method.
     * Otherwise, the bracelet will always be black and the temperature will be monitored in the background.
     */
    private void closeRealTemp() {
        YCBTClient.appTemperatureMeasure(0x00, new BleDataResponse() {
            @Override
            public void onDataResponse(int i, float v, HashMap hashMap) {
                if (i == 0) {
                    //success
                    Log.e("device", "Temp close");
                }
            }
        });
    }


    private BroadcastReceiver createChargingStateChangeReceiver(final EventChannel.EventSink events) {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

                if (status == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                    events.error("UNAVAILABLE", "Charging status unavailable", null);
                } else {
                    boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                            status == BatteryManager.BATTERY_STATUS_FULL;
                    events.success(isCharging ? "charging" : "discharging");
                }
            }
        };
    }


    private int getBatteryLevel() {
        int batteryLevel = -1;
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        } else {
            Intent intent = new ContextWrapper(getApplicationContext()).
                    registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            batteryLevel = (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100) /
                    intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        }

        Log.e("getbatt java", "Chama função post");
        event_handler.post(listeners.get(event_listener));
        Log.e("getbatt java", "Chamou...");

        return batteryLevel;
    }

    /**
     * Notificações #######################################
     *
     * Notificações para o status bar do Android
     *
     */
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        //unregister our receiver
        this.unregisterReceiver(this.broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra(Consts.MESSAGE_TEXT);
                //log our message value
                Log.i("Main Broadcast recebido", msg_for_me);

            }
        };
        //registering our receiver
        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SysUtils.logger(mContext, "Interface destruída", "destroy");
    }

    /*
    Cria o canal de comunicação do aplicativo
    */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Consts.SERVICE_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
