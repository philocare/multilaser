package com.philocare.philoscan;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class NotificationActivity extends Activity {

    private final Context ctx;

    public NotificationActivity(Context mCtx, Class classAct){
        this.ctx = mCtx;
    }

    public void send_note(String bannerTxt, String title, String txt, int icon){

        // -- Funcionando perfeitamente
        Uri soundUri = Uri.parse(String.valueOf(R.raw.beep));
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, Consts.SERVICE_CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(bannerTxt)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(txt))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(null)
                .setAutoCancel(true)
                .setSound(soundUri);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ctx);
        notificationManager.notify(12345, builder.build());
    }

}
