package com.philocare.philoscan;


import static com.philocare.philoscan.Consts.*;

import android.content.Context;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;

//import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.nio.CharBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SysUtils {

	public static final String NOT_FOUND = "NOT_FOUND";
	public static String pathConfig = "/philocare/";
	public static String configFileName = "config.cnf";
	public static String lastHRDateFileName = "lastHRDate.inf";
	public static String lastSleepDateFileName = "lastSleepDate.inf";
	public static String corruptDirectory = "/corr/";
	public static String LOG_FILE = "log_file";
	public static String CORR_DIR = "corr";
	public static String CONF_FILE = "cnf";

	/**
	 * Send the operation result logs to the logcat and TextView control on the UI
	 *
	 * @param string indicating the log string
	 * @param  tag activity log tag
	 */
	public static void logger(Context ctx, String string, String tag) {
		// salva log em disco
		saveLog(ctx, string, tag, LOG_FILE_NAME);
		Log.e(tag, string);
	}

	public static boolean deleteFile(Context ctx, String filename) {

		// Encontra o diretório de arquivos
		File filesDir = ctx.getFilesDir();
		String path = filesDir.getPath() + pathConfig;
		// pega a lista de arquivos

		File fdelete = new File(path, filename);
		if (fdelete.exists()) {
			return fdelete.delete();
		} else {
			return false;
		}
	}

	public static String[] readTxtFile(Context ctx) {

		String[] result = new String[2];
		// Encontra o diretório de arquivos
		File filesDir = ctx.getFilesDir();
		String path = filesDir.getPath() + pathConfig;
		// pega a lista de arquivos
		File directory = new File(path);

		FilenameFilter fltr = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if ((name.contains(LOG_FILE)) || (name.contains(CONF_FILE))) {
					return false;
				} else {
					return true;
				}
			}
		};

		File[] files = directory.listFiles(fltr);


		// verifica se tem arquivos
		if (files.length == 0) {
			// nenhum arquivo para enviar
			return null;
		}

		// Ordena para a mais antiga
		//Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);

		//Get the first text file
		File file = new File(path,files[0].getName());

//		if (Consts.DEBUG)
//			logger("Lendo arquivo: " + path + files[0].getName(), "read file" + sdcardDir,
//					Consts.LOG_FILE_NAME_DATA_POST);

		result[0] = files[0].getName();

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
			br.close();
		}
		catch (IOException e) {
			//You'll need to add proper error handling here
			return null;
		}

		result[1] = text.toString();

		return result;
	}

	/**
	 * 01/02/2022 - Robson
	 *
	 * Gravação de logs - O arquivo de log fica dentro da pasta de arquivos do sistema
	 * /data/data/com.philocare.philoscan/philocare
	 *
	 * Essa pasta é acessível usando o View->Tools Windows->Device Explorer do Android Studio
	 *
	 * @param ctx
	 * @param message
	 * @param tag
	 * @param log_file_name
	 */
	//Gravar string no arquivo de texto
	public static void saveLog(Context ctx, String message, String tag, String log_file_name) {

		try{
			File dir = new File(ctx.getFilesDir(), LOG_FILE_PATH_NAME);
			if(!dir.exists()){
				//Log.d(log_file_name, "Criando dir: " + ctx.getFilesDir() + "/philocare");
				dir.mkdir();
			}
			//Log.d(log_file_name, "Salvando log:" + dir.getName() + log_file_name);
			//File file = new File(ctx.getFilesDir() + "/" + log_file_name);
			File gpxfile = new File(dir +"/" + log_file_name);
			//Cada vez que você escreve, escreva em uma nova linha
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String now = sdf.format(new Date());
			String strContent = now + ": " + message + " TAG: " + tag + "\r\n";
			//Log.d(log_file_name, "Escrevendo no arquivo: " + gpxfile.getName() + " msg: " + strContent);
			FileWriter writer = new FileWriter(gpxfile, true);
			writer.append(strContent);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			Log.e(log_file_name, "Erro escrevendo no arquivo:" + e);
		}
	}

	/**
	 * 01/02/2022 - Robson
	 *
	 * Gravação de logs - O arquivo de log fica dentro da pasta de arquivos do sistema
	 * /data/data/com.philocare.philoscan/philocare
	 *
	 * Essa pasta é acessível usando o View->Tools Windows->Device Explorer do Android Studio
	 *
	 * @param ctx
	 *
	 */
	//Gravar string no arquivo de texto
	public static void saveData(Context ctx, String str_data, String file_name) {

		try{
			File dir = new File(ctx.getFilesDir(), FILE_PATH_NAME);
			if(!dir.exists()){
				//Log.d(log_file_name, "Criando dir: " + ctx.getFilesDir() + "/philocare");
				dir.mkdir();
			}
			//Log.d(log_file_name, "Salvando log:" + dir.getName() + log_file_name);
			//File file = new File(ctx.getFilesDir() + "/" + log_file_name);
			File gpxfile = new File(dir +"/" + file_name);
			//Cada vez que você escreve, escreva em uma nova linha
			//Log.d(log_file_name, "Escrevendo no arquivo: " + gpxfile.getName() + " msg: " + strContent);
			FileWriter writer = new FileWriter(gpxfile, true);
			writer.append(str_data);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			Log.e(file_name, "Erro escrevendo no arquivo:" + e);
		}
	}

	//Gravar string no arquivo de texto
	public static void writeTxtToFile(String devicecode, byte battery, String strcontent,
									  String filePath, String fileName, boolean overwrite) throws Exception {
		// Após gerar a pasta, gere o arquivo, caso contrário, ocorrerá um erro
		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + filePath;
		Calendar today = Calendar.getInstance();
		fileName = today.get(Calendar.YEAR) + "-" + (today.get(Calendar.MONTH) + 1) + "-" + today.get(Calendar.DAY_OF_MONTH) + "-" + fileName;
		makeFilePath(path, fileName);

		String strFilePath = path + fileName;
		//Cada vez que você escreve, escreva em uma nova linha
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = sdf.format(new Date());
		String strContent = devicecode + "@" + now + "@" + battery + "@" + strcontent + "\r\n";
		try {
			File file = new File(strFilePath);
			if (!file.exists()) {
				Log.d("TestFile", "Create the file:" + strFilePath);
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			RandomAccessFile raf = new RandomAccessFile(file, "rwd");
			if (!overwrite) {
				raf.seek(file.length());
			}
			raf.write(strContent.getBytes());
			raf.close();
		} catch (Exception e) {
			throw new Exception("Arquivo: " + fileName + " Erro gravando arquivo:" + e);
		}
	}

	// Gerar arquivo
	public static File makeFilePath(String filePath, String fileName) {
		File file = null;
		makeRootDirectory(filePath);
		try {
			file = new File(filePath + fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}

	//Gerar pasta
	public static void makeRootDirectory(String filePath) {
		File file = null;
		try {
			file = new File(filePath);
			if (!file.exists()) {
				file.mkdir();
			}
		} catch (Exception e) {
			Log.i("error:", e+"");
		}
	}

	/*
    Recupera o nome e código do dispositivo salvo no arquivo de preferências
    */
	public static String getSavedDevice (Context context) {

		String ret = "";
		String name_code = "";
		char[] buf = new char[256];

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			File gpxfile = new File(file, configFileName);
			FileReader reader = new FileReader(gpxfile);
			reader.read(buf);
			reader.close();

			name_code = new String(buf);

			name_code = name_code.trim();

		}catch (Exception e){
			e.printStackTrace();

		}

		return name_code;
	}

	/*
	Salva o nome e código da pulseira no arquivo de preferências
 	*/
	public static void saveDevice (Context context, String name, String code) throws IOException {

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			if(!file.exists()){
				file.mkdir();
			}
			// por enquanto vamos fazer apenas de um dispositivo
			File gpxfile = new File(file, configFileName);
			FileWriter writer = new FileWriter(gpxfile);
			writer.write(name + ';' + code);
			writer.flush();
			writer.close();

		}catch (Exception e){
			e.printStackTrace();

		}
	}

	/*
	Apaga o dispositivo salvo
 	*/
	public static boolean deleteSavedDevice(Context context) {
		try{
			File file = new File(context.getFilesDir() + pathConfig);
			if(!file.exists()){
				file.mkdir();
			}
			// por enquanto vamos fazer apenas de um dispositivo
			File gpxfile = new File(file, configFileName);
			if (gpxfile.exists()) {
				if (gpxfile.delete()) {
					//System.out.println("file Deleted :" + file);
					return true;
				} else {
					//System.out.println("file not Deleted :" +file);
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/*
	Salva a última data em arquivos de configuração
	 */
	public static void saveHRLastDate (Context context, String date) throws IOException {

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			if(!file.exists()){
				file.mkdir();
			}
			// por enquanto vamos fazer apenas de um dispositivo
			File gpxfile = new File(file, lastHRDateFileName);
			FileWriter writer = new FileWriter(gpxfile);
			writer.write(date);
			writer.flush();
			writer.close();

		}catch (Exception e){
			e.printStackTrace();

		}
	}

	/*
	Recupera a data do ultimo dado enviado
	*/
	public static String getHRSavedDate (Context context) {

		String ret = "";
		String last_date = "";
		char[] buf = new char[256];

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			File gpxfile = new File(file, lastHRDateFileName);
			if (file.exists()) {
				FileReader reader = new FileReader(gpxfile);
				reader.read(buf);
				reader.close();
				last_date = new String(buf);
				last_date = last_date.trim();
			} else {
				last_date = NOT_FOUND;
			}

		}catch (Exception e){
			e.printStackTrace();

		}

		return last_date;
	}

	/*
	Salva a última data em arquivos de configuração
	 */
	public static void saveSleepLastDate (Context context, String date) throws IOException {

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			if(!file.exists()){
				file.mkdir();
			}
			// por enquanto vamos fazer apenas de um dispositivo
			File gpxfile = new File(file, lastSleepDateFileName);
			FileWriter writer = new FileWriter(gpxfile);
			writer.write(date);
			writer.flush();
			writer.close();

		}catch (Exception e){
			e.printStackTrace();

		}
	}

	/*
	Recupera a data do último sono enviado
	*/
	public static String getSleepSavedDate (Context context) {

		String ret = "";
		String last_date = "";
		char[] buf = new char[256];

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			File gpxfile = new File(file, lastSleepDateFileName);
			FileReader reader = new FileReader(gpxfile);
			reader.read(buf);
			reader.close();

			last_date = new String(buf);

			last_date = last_date.trim();

		}catch (Exception e){
			e.printStackTrace();

		}

		return last_date;
	}

	/**
	 * Envia dados para a cloud
	 * @param context
	 */
	public static void doSendFiles(Context context)
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + "/philocare/";

		File directory = new File(path);
		File[] files = directory.listFiles();

		if (files != null) {
			// envia todos arquivos no dir philocare para o servidor
			for (File file : files) {
				StringBuffer sb = new StringBuffer("ftp://philo:7UT851@philocare.com/public_html/philocare.com/engine/huawei/" + file.getName());
				sb.append(";type=a");
				BufferedInputStream bis = null;
				BufferedOutputStream bos = null;
				try {
					URL url = new URL(sb.toString());
					URLConnection urlc = url.openConnection();

					bos = new BufferedOutputStream(urlc.getOutputStream());
					String filename = path + file.getName();
					bis = new BufferedInputStream(new FileInputStream(filename));

					int i;
					// read byte by byte until end of stream
					while ((i = bis.read()) != -1) {
						bos.write(i);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					// finalizou o envio, fecha as conexões
					if (bis != null)
						try {
							bis.close();
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
					if (bos != null)
						try {
							bos.close();
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
					// apaga o arquivo enviado
					file.delete();
				}
			}
		}
	}

	public static boolean moveFile(String filename) {
		// Encontra o diretório de arquivos
		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + pathConfig;

		//makeFilePath(path + corruptDirectory, filename);

		// pega a lista de arquivos
		// Verifica a existencia do diretório
		File path_corr = new File(path +  corruptDirectory);
		if (!path_corr.exists()) {
			File file = new File(sdcardDir + corruptDirectory);
			if (!file.exists()) {
				file.mkdir();
			}
		}

		File from = new File(path + filename);
		if (from.exists()) {
			File to = new File(path + corruptDirectory, filename);
			return from.renameTo(to);
		} else {
			return false;
		}
	}
}




// Get HiHealthPointType, like steps, distance, calories, exercise intensity per day.
// Statistic data returned as an ArrayList where each element represents the value of one day
//HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(HiHealthPointType.DATA_POINT_STEP_SUM, startTime,
//        endTime, new HiHealthDataQueryOption());
        /*
        Retorna o total de passos
        public static final int DATA_POINT_STEP_SUM = 40002;

        Retorna a distância percorrida (passos x tamanho do passo)
        public static final int DATA_POINT_DISTANCE_SUM = 40004;

        Retorna as calorias gastas de acordo com o número de passos
        public static final int DATA_POINT_CALORIES_SUM = 40003;

        Não retornou nada
        public static final int DATA_POINT_EXERCISE_INTENSITY = 47101;

        Não retornou nada
        public static final int DATA_POINT_HEALTH_MIN = 2000;

        Não retornou nada
        public static final int DATA_POINT_REST_HEARTRATE = 2018;

        Não retornou nada
        public static final int DATA_POINT_ALTITUDE_OFFSET_SUM = 40005;

        Não retornou nada
        public static final int DATA_POINT_EXERCISE_INTENSITY = 47101;

        Não retornou nada
        public static final int DATA_POINT_MIN = 1;

        Não retornou nada
        public static final int DATA_SET_MIN = 10000;

        DATA_SET_HEART - retorna os batimentos diários mínimo, máximo e de repouso
        DATA_SET_CORE_SLEEP - retorna o sono por dia:

        REM sleep duration - 44101 - em minutos
        Deep sleep duration of the day - 44102 - em minutos
        Light sleep duration of the day - 44103 - em minutos
        Total sleep duration of the day - 44105 - em minutos
        Deep sleep continuity - 44106 - em minutos
        Number of times the user is awake - 44107 - times
        Dormiu time - 44201 - em minutos
        Acordou time - 44202 - em minutos
        Score - 44203 - score
        Sleep duration at night - 44209 - em minutos

        Não retornou nada
        public static final int DATA_SET_WALK_METADATA = 30005;

        Não retornou nada
        public static final int DATA_SET_RUN_METADATA = 30006;

        Retorna os dados de uma atividade esportiva realizada
        public static final int DATA_SET_RIDE_METADATA = 30007;

         */