/**
 *
 * Flitter - Acesso a biblioteca java do SDK da pulseira fornecida pela multilaser
 *
 * Data: 27/01/2022
 *
 * Robson Pierangeli Godinho
 */

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:philoscan/events.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);



  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Acesso às smartbands',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Philocare acesso via SDK'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  static const platform = MethodChannel('philocare.flutter.dev/sdk');

  // static const EventChannel eventChannel = EventChannel('philocare.flutter.dev/sdk_evt');

  // Get battery level.
  String _macAdress = 'Sem status.';
  String _macText = '00:00:00:00';
  String _connectStatus = 'Desconectado';
  String _hrValue = 'Nenhum';
  String _tempValue = 'Nenhum';
  String _sleepValue = 'Nenhum';
  String _delmacText = '----';
  String _postres = '----';
  String _batteryLevel = '----';

  String _deviceBatteryLevel = 'Battery level: unknown.';
  String _chargingStatus = 'Battery status: unknown.';

  Future <void> _sendData() async {
    String res;
    try {
      final String result = await platform.invokeMethod('sendpost');
      res = 'Java resp: $result.';
    } on PlatformException catch (e) {
      res = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _postres = res;
    });
  }

  Future<void> _sendMessageToDevice() async {
    String value;
    try {
      value = await platform.invokeMethod(
        "sendmsg",
        <String, dynamic>{
          'message': "ASPIRINA 500mg",
        },
      );
      print(value);
    } catch (e) {
      print(e);
    }
  }

  Future<void> _startScan() async {
    String macadress;
    try {
      final String result = await platform.invokeMethod('startscan');
      macadress = 'Java resp: $result.';
    } on PlatformException catch (e) {
      macadress = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _macAdress = macadress;
    });
  }

  @override
  void initState() {
    super.initState();
    runPlayground();
  }
  //
  // void _onEvent(Object? event) {
  //   setState(() {
  //     _chargingStatus =
  //     "Battery status: ${event == 'charging' ? '' : 'dis'}charging.";
  //   });
  // }
  //
  // void _onError(Object error) {
  //   setState(() {
  //     _chargingStatus = 'Battery status: unknown.';
  //   });
  // }


  Future<void> _getMac() async {
    String macText;
    try {
      final String result = await platform.invokeMethod('getmac');
      macText = 'Java resp: $result.';
    } on PlatformException catch (e) {
      macText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _macText = macText;
    });
  }

  Future<void> _deleteMAC() async {
    String delmacText;
    try {
      final String result = await platform.invokeMethod('deletemac');
      delmacText = 'Java resp: $result.';
    } on PlatformException catch (e) {
      delmacText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _delmacText = delmacText;
    });
  }

  Future<void> _getConnect() async {
    String connectText;
    try {
      final String result = await platform.invokeMethod('connect');
      connectText = 'Java resp: $result.';
    } on PlatformException catch (e) {
      connectText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _connectStatus = connectText;
    });
  }

  Future<void> _getDeviceBatteryLevel() async {
    String batteryText;
    try {
      final String result = await platform.invokeMethod('getDeviceBatteryLevel');
      batteryText = '$result.';
    } on PlatformException catch (e) {
      batteryText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _deviceBatteryLevel = batteryText;
    });
  }

  Future<void> _getBatteryLevel() async {

    String batteryText = "---";

    try {
      final String result = await platform.invokeMethod('getBatteryLevel');
      batteryText = 'Resp: $result.';
    } on PlatformException catch (e) {
      batteryText = "Java falhou: '${e.message}'.";
    }

    // if (running) return;
    // running = true;
    //
    // var cancel = startListening((msg) {
    //   setState(() {
    //     print('${DateTime.now()} - Flutter');
    //     batteryText = msg;
    //     print('${msg} - do java');
    //     log(msg);
    //   });
    // });
    //
    // await Future.delayed(const Duration(minutes: 1));
    //
    // cancel();
    //
    // running = false;

    setState(() {
      _batteryLevel = batteryText;
    });
  }


  Future<void> _getTemp() async {
    String tempText;
    try {
      final String result = await platform.invokeMethod('gettemp');
      tempText = 'Java resp: $result.';
    } on PlatformException catch (e) {
      tempText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _tempValue = tempText;
    });
  }

  Future<void> _getHR() async {
    String hrText;
    try {
      final String result = await platform.invokeMethod('gethr');
      hrText = 'Java resp: $result.';
    } on PlatformException catch (e) {
      hrText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _hrValue = hrText;
    });
  }

  Future<void> _getSleep() async {
    String hrText;
    try {
      final String result = await platform.invokeMethod('getsleep');
      hrText = 'Java resp: $result.';
    } on PlatformException catch (e) {
      hrText = "Java falhou: '${e.message}'.";
    }

    setState(() {
      _sleepValue = hrText;
    });
  }

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: const Text('Iniciar SCAN'),
              onPressed: _startScan,
            ),
            Text(
              '$_macAdress',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Conectar'),
              onPressed: _getConnect,
            ),
            Text(
              '$_connectStatus',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Temperatura'),
              onPressed: _getTemp,
            ),
            Text(
              '$_tempValue',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Batimentos'),
              onPressed: _getHR,
            ),
            Text(
              '$_hrValue',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Sono'),
              onPressed: _getSleep,
            ),
            Text(
              '$_sleepValue',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Apaga pulseira registrada'),
              onPressed: _deleteMAC,
            ),
            Text(
              '$_delmacText',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Busca estado da bateria'),
              onPressed: _getDeviceBatteryLevel,
            ),
            Text(
              '$_deviceBatteryLevel',
              style: Theme.of(context).textTheme.caption,
            ),
            ElevatedButton(
              child: const Text('Envia mensagem'),
              onPressed: _sendMessageToDevice,
            ),
            ElevatedButton(
              child: const Text('Envia dados'),
              onPressed: _sendData,
            ),
            Text(
              '$_postres',
              style: Theme.of(context).textTheme.caption,
            ),
          ],
        ),
      ),

      floatingActionButton: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: runPlayground,
            tooltip: 'Run Playground',
            child: const Icon(Icons.play_arrow),
          ),
        ],
      ),
    );

    // return Material(
    //   child: Center(
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //       children: [
    //         ElevatedButton(
    //           child: const Text('Iniciar SCAN'),
    //           onPressed: _getBatteryLevel,
    //         ),
    //         c,
    //         ElevatedButton(
    //           child: const Text('Buscar MAC'),
    //           onPressed: _getMac,
    //         ),
    //         Text(_macText),
    //       ],
    //     ),
    //
    //   ),
    // );
  }

  /////////////// Playground ///////////////////////////////////////////////////
  String logs = "";

  // Call inside a setState({ }) block to be able to reflect changes on screen
  void log(String logString) {
    logs += logString.toString() + " ${DateTime.now()} \n";
  }

  bool running = false;
  void runPlayground() async {
    if (running) return;
    running = true;

    var cancel = startListening((msg) {
      setState(() {
        print('${DateTime.now()} - Flutter');
        print('${msg} - do java');
        //log(msg);
        _deviceBatteryLevel = msg;
      });
    });

    //await Future.delayed(const Duration(minutes: 1));

    // cancel();
    //
    // running = false;
  }
}


